/* Copyright (c) 2012 Oleg Goncharov
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * ttpa/ttpa_timer.c
 *      timer handling for TTP/A protocol 
 *
 * Revision History:
 *   2012/12/14 (0.8): O. Goncharov
 *     - timer handling is moved from ttpa.c. 
 *     - Support for timer with fixed positon of ttpa slot: at start of slot set timer to 0.
 *     - Support for 8 bit timer.
 *   2012/12/14 (0.10): O. Goncharov
 *     - timer overflow detection: one and only one call of ttpa_timer_nextslot is possible 
 *     between two ISR(TOVF) calls
 *     - ttpa_timer_set_ocr_safe
 */

#include <avr/interrupt.h>

#include "node_config.h"

#include "ttpa_timer.h"
#include "debug.h"

volatile timerval_t ttpa_slotlength;
volatile timerval_t ttpa_bitlength;
volatile uint8_t ttpa_ovcnt;

#if defined(TIMER_FIXEDSLOT)
timerval_t ttpa_timer_event_recvstart; // recive op. setup time
timerval_t ttpa_timer_event_sendstart; // send op. start time
timerval_t ttpa_timer_event_execstart; // exec op. start time
timerval_t ttpa_timer_event_startb_to; // recive op. startb timeout
timerval_t ttpa_timer_event_op_to; // op. in current slot timeout
timerval_t ttpa_timer_event_minslotend; // slotend event

volatile uint8_t ttpa_timer_nextslot_cnt;

# if TIMER_DIV != 1
ttpa_timer_8b_correction_t ttpa_timer_8b_corr;
# endif
#endif

#if defined(TIMER_FLOATSLOT)
volatile timerval_t ttpa_slotstart;
#endif

/**
 * Start timer.
 */
void ttpa_timer_init(void) {
	ttpa_timer_set_slotlength(SLOTLENGTH_NOMINAL, BITLENGTH_NOMINAL);

	TTPA_TCNT = 0x00;
	ttpa_ovcnt = 0x00;
	//set clock and prescaler
#if (TIMER_DIV) == 1 
	TTPA_TCCRB = _BV(TTPA_CS0);
#elif (TIMER_DIV) == 8
	TTPA_TCCRB = _BV(TTPA_CS1);
#elif (TIMER_DIV) == 32
	TTPA_TCCRB = _BV(TTPA_CS1) | _BV(TTPA_CS0);
#elif (TIMER_DIV) == 64
	TTPA_TCCRB = _BV(TTPA_CS2);
#elif (TIMER_DIV) == 128
	TTPA_TCCRB = _BV(TTPA_CS2) | _BV(TTPA_CS0);
#endif
	// select timer/counter operationsl mode
#if defined (TIMER_FIXEDSLOT)
	ttpa_timer_nextslot_cnt = 0;
	// CTC: TOP value is equal ttpa_slotlength-1 
	TTPA_TCTCR |= _BV(TTPA_CTC);
	TTPA_TTOP = ttpa_slotlength - 1;
#endif
	// enable overflow interrupt
	TTPA_TIFR = _BV(TTPA_TOIE);
	TTPA_TIMSK |= _BV(TTPA_TOIE);
}

/**
 * Setup OC interrupt on ocr_var, enable it and return 0.
 * If interrupt will not be triggered in margin timer cycles,
 * disable OC interrupt and return 1.
 */
uint8_t ttpa_timer_set_ocr_safe(timerval_t ocr_val, timerval_t margin) 
{
	uint8_t tmp = SREG;
	uint8_t ret;
	timerval_t tcnt;
	timerval_t delay;

	//disable interrupts
	cli();
	// set OC register value
	TTPA_TIFR = _BV(TTPA_OCIE);
	TTPA_TIMSK |= _BV(TTPA_OCIE);
	TTPA_OCR = ocr_val;
	tcnt = TTPA_TCNT;

	if (TTPA_TIFR & _BV(TTPA_OCIE)) {
		// interrupt have been triggered
		ret = 0;
	}
	else {
		// Interrupt has not been triggered yet.
		// Ensure it will be triggered in margin timer cycles
#if defined(TIMER_FIXEDSLOT)
		if (ocr_val <= tcnt) ocr_val += TTPA_TTOP;
#endif /* TIMER_FIXEDSLOT */
		delay = ocr_val - tcnt;
		if (0 < delay && delay < margin) ret = 0;
		else ret = 1;
	}
	// if 
	if (ret) TTPA_TIMSK &= ~_BV(TTPA_OCIE);
	// restore interrupt state
	SREG = tmp;
}

/*
 * ISR(TTPA_SIG_TOV): signal handler for overflow
 */
ISR(TTPA_SIG_TOV)
{
#if defined(TIMER_FIXEDSLOT)
	//setup TOP value
# if TIMER_DIV == 1
	TTPA_TTOP = ttpa_slotlength - 1;
# else
	//perform TOP correction 
	ttpa_timer_8b_corr.remainer += ttpa_timer_8b_corr.slotshift;
	if (ttpa_timer_8b_corr.remainer > TIMER_DIV) {
		ttpa_timer_8b_corr.remainer -= TIMER_DIV;
		TTPA_TTOP = ttpa_slotlength;
	}
	else {
		TTPA_TTOP = ttpa_slotlength - 1;
	}
# endif
	//check if inside slot was only one ttpa_timer_nextslot call
	//if OC interrupt disabled do nothing: we are waiting for external event
	if (ttpa_timer_nextslot_cnt != 1 &&	(TTPA_TIMSK & _BV(TTPA_OCIE))) {
		if (ttpa_timer_nextslot_cnt == 0) DPUTC('v');
		else DPUTC('V');
		//DPRINT1("%d", ttpa_timer_nextslot_cnt);
		//ttpa_setstate(TTPA_STATE_UNSYNC);
	}
	ttpa_timer_nextslot_cnt = 0;
#endif /* TIMER_FIXEDSLOT */
	ttpa_ovcnt++;
	//DPUTC('T');
#if defined(PROFILE_TIMER_OVF) 
	if (ttpa_ovcnt == 0) debug_profilefile.tovf_vect = 0;
#endif
}

/**
 * Set slotlength and bitlength. Recalculate timer events moments.
 */
void ttpa_timer_set_slotlength(uint16_t slotlength, uint16_t bitlength)
{
	ttpa_slotlength = slotlength / TIMER_DIV;
	ttpa_bitlength = bitlength / TIMER_DIV;
#if defined(TIMER_FIXEDSLOT)
	ttpa_timer_event_recvstart = ttpa_slotlength - ttpa_bitlength + UART_RECVCORR/TIMER_DIV - 1;
#if UART_SENDCORR/TIMER_DIV >= 0
	ttpa_timer_event_sendstart = UART_SENDCORR/TIMER_DIV;
#else
	ttpa_timer_event_sendstart = ttpa_slotlength + UART_SENDCORR/TIMER_DIV; 
#endif
	ttpa_timer_event_execstart = 0;
	ttpa_timer_event_startb_to = ttpa_bitlength;
//TODO Set operaion timeout to 12 bit. 
	ttpa_timer_event_op_to = (24*bitlength/2 ) / TIMER_DIV;
	ttpa_timer_event_minslotend = ttpa_timer_event_recvstart - 2u*ttpa_bitlength;
	
# if TIMER_DIV != 1
	ttpa_timer_8b_corr.slotshift = slotlength % TIMER_DIV;
	ttpa_timer_8b_corr.remainer = 0;
# endif
	/*DPRINT1("SL:%d", slotlength);
	DPRINT1("ST:%d", ttpa_timer_event_startb_to);
	DPRINT1("OT:%d", ttpa_timer_event_op_to);
	DPRINT1("RS:%d", ttpa_timer_event_recvstart);
	DPRINT1("SS:%d", ttpa_timer_event_sendstart);
	DPRINT1("SE:%d", ttpa_slotlength);*/
#endif /* TIMER_FIXEDSLOT */
}

/**
 * Stop timer.
 */
void ttpa_timer_disable(void) 
{
	// set clock (no prescaling)
	TTPA_TCCRB = 0;

	TTPA_TIFR |= (1<<TTPA_TOIE);
	TTPA_TIMSK &= ~(1<<TTPA_TOIE);
}

