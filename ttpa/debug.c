/* Copyright (c) 2012? Olrg Goncharov
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * ttpa/debug.c
 *      IFS file for debugging and profiling
 *
 * Revision History:
 *   2012/12/22 (0.9): O. Goncharov
 *    - PROFILE and timer overflow support
 *
 */
#include <avr/io.h>
#include <stdio.h>

#include "ifs.h"
#include "ifs_types.h"
#include "debug.h"

#if defined(DEBUG_PUTCHAR)

static int debug_putchar(char c, FILE *stream);
static int debug_getchar(FILE *stream);

static FILE debug_uart = FDEV_SETUP_STREAM(debug_putchar, debug_getchar, _FDEV_SETUP_RW);

static int debug_putchar(char c, FILE *stream) 
{
  loop_until_bit_is_set(UCSR0A, UDRE0);
  UDR0 = c;
  return 0;
}

static int debug_getchar(FILE *stream) 
{
	int c;
	loop_until_bit_is_set(UCSR0A, RXC0);
	if (UCSR0A & (_BV(FE0)|_BV(UPE0)|_BV(DOR0))) {
		c = UDR0;
		return _FDEV_ERR;
	} 
	else return UDR0;
}

void debug_init(void) 
{
#	if defined(BAUD_U2X)
#		define DEBUG_UBRREG_VALUE ((uint16_t)((CLOCKRATE + 4UL*DEBUG_BAUDRATE)/(8UL*DEBUG_BAUDRATE) - 1UL))
#	else
#		define DEBUG_UBRREG_VALUE ((uint16_t)((CLOCKRATE + 8UL*DEBUG_BAUDRATE)/(16UL*DEBUG_BAUDRATE) - 1UL))
#	endif

	UBRR0 = DEBUG_UBRREG_VALUE;

	UCSR0A = 0;
	UCSR0B = _BV(TXEN0) | _BV(RXEN0);
	UCSR0C =  _BV(UCSZ00) | _BV(UCSZ01);

	stdout = &debug_uart;
	stdin = &debug_uart;
}
#endif

#if defined(PROFILE)

struct debug_profilefile_struct debug_profilefile;

IFS_ADDAPPLFILE(DEBUG_SYSFILE_PROFILE, &debug_profilefile, NULL, IFS_FILELEN(struct debug_profilefile_struct), ifs_int_0, 066);

#  if defined(PROFILE_TIMER_OVF)
uint8_t debug_ovcnt_expected_val;
#  endif

#endif /* PROFILE */

