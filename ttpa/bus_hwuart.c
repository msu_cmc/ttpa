 /* Copyright (c) 2004, 2005, Christian Trödhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * ttpa/bus_hwuart.c
 *      general functions for HW UART
 *
 * $Id: bus_hwuart.c,v 1.0 bernd Exp $
 *
 * Revision History:
 *   2012/11/23 (0.1): O. Goncharov
 *     Drop-in replacment for bus_swuart. 
 *     startb timing detection is based on RX pin PCINT.
 *     Timeout handling for recv and sed.
 *   2012/11/23 (0.2): O. Goncharov
 *     Frame oriented operations. Compatibility with old version is broken.
 *   2012/12/22 (0.8): O. Goncharov
 *     New timer interface.
 *   2012/12/22 (0.9): O. Goncharov
 *     PROFILE support. If IC is available, calculates CORR values.
 *   2012/12/30 (0.10): O. Goncharov
 *     - potential bug with early triggered URDE interrupt is fixed
 *     - added bus_set_baudrate() function
 */
#include <avr/interrupt.h>
#include <util/parity.h>

#include "node_config.h"
#include "node_swconfig.h"
#include "ttpa.h"
#include "ttpa_timer.h"
#include "bus.h"
#include "bus_hwuart.h"
#include "bus_transcvr.h"
#include "schedule.h"
#include "debug.h"

volatile bus_hwuart_buf_t bus_hwuart_buf;

//TODO: volatile ?
volatile uint8_t bus_io_buffer[1<<MAXSPDUP];

volatile uint16_t bus_timer_startb;

uint16_t bus_hwuart_brreg_value[MAXSPDUP + 1];

// handler for startb interrupt
void (* volatile bus_sig_startb) (void);

// callback: bus operation complete
void (* volatile bus_op_complete) (bus_iobuf_t *param); 

/*
 * void bus_init(void): Initialize HW UART
 */
int bus_init(void)
{
	//DPRINT("hw_bus_init\n");
	//TODO: wait until transmition is complete
	//loop_util_bit_is_set(HW_UART_STATREGA, HW_UART_TXC);
	bus_set_baudrate(SLOTLENGTH_NOMINAL, BITLENGTH_NOMINAL);
	// Set mode 
#ifdef BAUD_U2X
	HW_UART_STATREGA = _BV(HW_UART_U2X);
#else 
	HW_UART_STATREGA = 0;
#endif
	// 9 data bits, 1 stop bits, no parity
	HW_UART_STATREGB = _BV(HW_UART_UCSZ2);
	HW_UART_STATREGC = _BV(HW_UART_UCSZ0) | _BV(HW_UART_UCSZ1);

	// setup startb interrupt
	BUS_SIG_RXSTART_MSKREG |= _BV(BUS_SIG_RXSTART_MSK);

	// disable interrupts
	BUS_SIG_RXSTART_ICREG &= ~_BV(BUS_SIG_RXSTART_IE);
	HW_UART_STATREGB &= ~_BV(HW_UART_RXCIE);
	HW_UART_STATREGB &= ~_BV(HW_UART_UDRIE);
	HW_UART_STATREGB &= ~_BV(HW_UART_TXCIE);
	
	//setup syncronous mode 
#if defined(HW_UART_SYNCRONOUS)
	HW_UART_STATREGC |= _BV(HW_UART_UMSEL0);
	if(ttpa_conf_master()) {
		HW_UART_XCKDDR |= _BV(HW_UART_XCKPIN);
	} 
	else {
		HW_UART_XCKDDR &= ~_BV(HW_UART_XCKPIN);
	}
#endif

	//disable tranciver and reciver
	HW_UART_STATREGB |= _BV(HW_UART_TXEN);	
	HW_UART_STATREGB &= ~_BV(HW_UART_RXEN);	

#if defined(PROFILE)
	// setup test IC: falling edge
	//TTPA_TCCRB |= _BV(TTPA_ICNC);
	TTPA_TCCRB &= ~_BV(TTPA_ICES);
	TTPA_TIMSK &= ~_BV(TTPA_TICIE);
#endif /*PROFILE*/
	//init transiver
	bus_transcvr_init();
	return 0;
}

ADD_INITTASK(task_bus_init, bus_init, 2, (1<<TTPA_STATE_UNSYNC));

/*
 * int bus_error(void): disable uart on error
 */
int bus_error(void)
{
	bus_transcvr_recv();
	// disable transmiter and reciver
	HW_UART_STATREGB &= ~_BV(HW_UART_TXEN);	
	HW_UART_STATREGB &= ~_BV(HW_UART_RXEN);	

	// disable interrupts
	BUS_SIG_RXSTART_ICREG &= ~_BV(BUS_SIG_RXSTART_IE);
	HW_UART_STATREGB &= ~_BV(HW_UART_RXCIE);
	HW_UART_STATREGB &= ~_BV(HW_UART_UDRIE);
	HW_UART_STATREGB &= ~_BV(HW_UART_TXCIE);
#if defined(PROFILE)
	TTPA_TIMSK &= ~_BV(TTPA_TICIE);
#endif /*PROFILE*/
	return 0;
}

ADD_INITTASK(task_bus_error, bus_error, 2, (1<<TTPA_STATE_ERROR));

/*
 * void bus_set_baudrate(uint16_t slotlength, uint16_t bitlength)
 * Set UART baurdrate.
 */
void bus_set_baudrate(uint16_t slotlength, uint16_t bitlength)
{
	uint8_t i;
#if defined(BAUD_U2X) 
	uint8_t shift = 3;
#else 
	uint8_t shift = 4;
#endif
	bus_hwuart_brreg_value[0] = bitlength >> shift; // bitlength / 8
	bus_hwuart_brreg_value[2] = bus_hwuart_brreg_value[0] / 5; 
	for(i = 3; i <= MAXSPDUP; i++) {
		bus_hwuart_brreg_value[i] = bus_hwuart_brreg_value[i-1] >> 1; 
	}
	HW_UART_BRREG = bus_hwuart_brreg_value[0];
	//DPRINT2("BR:%d %d\n", bus_hwuart_brreg_value[0], bus_hwuart_brreg_value[2]);
}


/*
 * void bus_recvframe_init(bus_iobuf_t *param): Initialize HW UART for receive
 * operation in next slot
 */
void bus_recvframe_init(bus_iobuf_t *param)
{
	bus_hwuart_buf.brreg_value = bus_hwuart_brreg_value[param->spdup];
	bus_hwuart_buf.timeout = param->timeout;
	bus_hwuart_buf.sync = param->sync;
	bus_hwuart_buf.bytecnt = 1 << param->spdup;
	bus_hwuart_buf.buf = param->buf;
	bus_hwuart_buf.buf_ptr = param->buf;
	bus_hwuart_buf.par_odd = param->fb_byte;
	// set OC handler
	ttpa_sig_oc = (void (*)(void)) bus_recvframe_setup;
	// set startb interrupt handler
	bus_sig_startb = (void (*)(void)) bus_recvbyte_startb;
	// set timer
	ttpa_timer_set_ocr(ttpa_timer_event_recvstart);
#if defined(PROFILE)
	debug_setup_tovf_check(0);
#endif /*PROFILE*/
}

/* 
 * bus_recvframe_setup(): setup of receive one BC before expected start
 */
void bus_recvframe_setup(void)
{
	//set baudrate
	HW_UART_BRREG = bus_hwuart_buf.brreg_value;	
	// enable reciver
	HW_UART_STATREGB |= _BV(HW_UART_RXEN);
	// enable STARTB interrupt
	BUS_SIG_RXSTART_IFREG = _BV(BUS_SIG_RXSTART_IF);
	BUS_SIG_RXSTART_ICREG |= _BV(BUS_SIG_RXSTART_IE);
#if defined(PROFILE)
	{
		timerval_t tmp = TTPA_TCNT;
		debug_profilefile.recv_corr = INT16_TO_IFS(TTPA_OCR - tmp);
		debug_check_tovf(1);
		if (debug_profilefile.tovf_vect & 1) {
			DPUTC('L');
			debug_profilefile.tovf_vect &= ~1;
		}
	}
#endif /*PROFILE*/
	// set timeout
	if(bus_hwuart_buf.timeout) {
		// set OC handler
		ttpa_sig_oc = (void (*)(void)) bus_recvframe_to;
		// set timeout 
		ttpa_timer_set_ocr(ttpa_timer_event_startb_to);
#if defined(PROFILE)
		debug_setup_tovf_check(1);
#endif /*PROFILE*/
	} else {
		ttpa_timer_oc_disable();
	}
	// enable RX complete interrupt
	HW_UART_STATREGB |= _BV(HW_UART_RXCIE);
}

/*
 *  ISR(BUS_SIG_RX_INT): signal handler for startb recive interrupt (IC is not available)
 */
ISR(BUS_SIG_RXSTART_INT)
{ 
#if defined(PROFILE)
	timerval_t bus_timer_ic = TTPA_ICR;
#endif /*PROFILE*/
	// timer syncronization
	bus_timer_startb = TTPA_TCNT;
	if (bus_hwuart_buf.sync) {
		// sync timer with master
#if defined(TIMER_FLOATSLOT)
		ttpa_timer_sync(bus_timer_startb, BUS_SIG_RXSTART_INT_CORR);
#endif
#if defined(TIMER_FIXEDSLOT)
		ttpa_timer_sync(0, BUS_SIG_RXSTART_INT_CORR);
#endif
	}
	// get IC test value
#if defined(PROFILE)
	debug_profilefile.startb_corr = INT16_TO_IFS(bus_timer_ic - bus_timer_startb);
	debug_profilefile.recv_delay = INT16_TO_IFS(bus_timer_ic - ttpa_timer_event_slotstart);
#endif /*PROFILE*/
	
	// disable startb interrupt
	BUS_SIG_RXSTART_IFREG = _BV(BUS_SIG_RXSTART_IF);
	BUS_SIG_RXSTART_ICREG &= ~_BV(BUS_SIG_RXSTART_IE);

	(*bus_sig_startb)();
}

/*
 *  void bus_recvbyte_startb(void): default handler for startb recive interrupt (IC is not available)
 */
void bus_recvbyte_startb(void)
{
	//DPUTC('_');
	if (bus_hwuart_buf.timeout) {
		// setup timeout	
		ttpa_timer_set_ocr(ttpa_timer_event_op_to);
	}
}

/*
 *  ISR(HW_UART_SIG_RXC): signal handler for recive complete interrupt 
 */
ISR(HW_UART_SIG_RXC)
{ 
#if defined(PROFILE)
	debug_profilefile.recv_end = INT16_TO_IFS(TTPA_TCNT);
#endif /* PROFILE */
	if (HW_UART_STATREGA & _BV(HW_UART_FE)) {		// frame error
		bus_iobuf_t buf;
	
		buf.error =  BUS_IO_FE;
		// disable reciver and RX complete interrupt
		HW_UART_STATREGB &= ~_BV(HW_UART_RXEN);
		HW_UART_STATREGB &= ~_BV(HW_UART_RXCIE);
		// UART ready -> call op_complete callback
		(*bus_op_complete)(&buf);
	}
	else {
		uint8_t par;
		uint8_t byte;

		// get parity bit
		if (bus_hwuart_buf.par_odd) par = (HW_UART_STATREGB & _BV(HW_UART_RXB8)) ? 0x00 : 0x01;
		else par = (HW_UART_STATREGB & _BV(HW_UART_RXB8)) ? 0x01 : 0x00;
	    byte = HW_UART_DATAREG;

		// check parity
		if  (par != parity_even_bit(byte)) {
			bus_iobuf_t buf;

			buf.error = BUS_IO_PE;
			// disable reciver and RX complete interrupt
			HW_UART_STATREGB &= ~_BV(HW_UART_RXEN);
			HW_UART_STATREGB &= ~_BV(HW_UART_RXCIE);
			// UART ready -> call op_complete callback
			(*bus_op_complete)(&buf);
		}
		else {
			*bus_hwuart_buf.buf_ptr++ = byte;
			bus_hwuart_buf.bytecnt--;

			// check if op. is comlete 
			if (bus_hwuart_buf.bytecnt == 0) {
				bus_iobuf_t buf;

				buf.error = BUS_IO_OK;
				buf.buf = bus_hwuart_buf.buf;
				//buf.slotstart = bus_timer_startb;
				
				// disable timeout interrupt
				ttpa_timer_oc_disable();

				// disable reciver and RX complete interrupt
				HW_UART_STATREGB &= ~_BV(HW_UART_RXEN);
				HW_UART_STATREGB &= ~_BV(HW_UART_RXCIE);
				// UART ready -> call op_complete callback
				(*bus_op_complete)(&buf);
			}
		}
	}
}

/*
 * void bus_recvframe_to(void): Receive operation just timed out; set error
 * code and jump to callback
 */
void bus_recvframe_to(void)
{
	bus_iobuf_t buf;

#if defined(PROFILE)
	if (bus_hwuart_buf.sync == 0) debug_check_tovf(4);
#endif /* PROFILE */

	if (TTPA_OCR == ttpa_timer_event_startb_to) { DPUTC('K'); }
	else { DPUTC('R'); }

	buf.error = BUS_IO_TIMEOUT;
	// disable reciver
	HW_UART_STATREGB &= ~_BV(HW_UART_RXEN);
	// disable startb and RX complete interrupt
	BUS_SIG_RXSTART_ICREG &= ~_BV(BUS_SIG_RXSTART_IE);
	HW_UART_STATREGB &= ~_BV(HW_UART_RXCIE);
	// UART ready -> call op_complete callback
	(*bus_op_complete)(&buf);
}

/*
 * void bus_sendframe_init(bus_iobuf_t *param): Prepear for transmission
 */
void bus_sendframe_init(bus_iobuf_t *param)
{
	bus_hwuart_buf.brreg_value = bus_hwuart_brreg_value[param->spdup];
	bus_hwuart_buf.timeout = param->timeout;
	bus_hwuart_buf.bytecnt = 1 << param->spdup;
	bus_hwuart_buf.buf = param->buf;
	bus_hwuart_buf.buf_ptr = param->buf;
	bus_hwuart_buf.par_odd = param->fb_byte;
	// set OC handler
	ttpa_sig_oc = (void (*)(void)) bus_sendframe;
	// set timer
	ttpa_timer_set_ocr(ttpa_timer_event_sendstart);
#if defined(PROFILE)
	debug_setup_tovf_check(0);
#endif /*PROFILE*/
}

/*
 * void bus_sendframe(void): setup transmission and transmit first byte
 */
void bus_sendframe(void)
{
	uint8_t byte = *bus_hwuart_buf.buf_ptr;
#if defined(PROFILE)
	// enable IC for send_corr test 
	TTPA_TIFR = _BV(TTPA_TICIE);
	TTPA_TIMSK |= _BV(TTPA_TICIE);
	debug_check_tovf(2);
#endif /*PROFILE*/
	// set transceiver direction to send
	bus_transcvr_send();
	//set baudrate
	HW_UART_BRREG = bus_hwuart_buf.brreg_value;	
	// put data in data reg
	if (bus_hwuart_buf.par_odd) {
		if (0 == parity_even_bit(byte)) HW_UART_STATREGB |= _BV(HW_UART_TXB8);
		else HW_UART_STATREGB &= ~_BV(HW_UART_TXB8);
	}
	else {
		if (1 == parity_even_bit(byte)) HW_UART_STATREGB |= _BV(HW_UART_TXB8);
		else HW_UART_STATREGB &= ~_BV(HW_UART_TXB8);
	}
	HW_UART_DATAREG = byte;
	bus_hwuart_buf.buf_ptr++;
	bus_hwuart_buf.bytecnt--;
	// clear TXC flag
	HW_UART_STATREGA |= _BV(HW_UART_TXC);

	// There is a small possibility, that UDRE will be triggered BEFORE slotstart,
	// so ttpa_nextframe can be called not inside current slot.
	// If TIMER_FIXEDSLOT is defined this can cause slot lost.
#if defined (TIMER_FIXEDSLOT) 
	// set OC handler
	ttpa_sig_oc = (void (*)(void)) bus_sendframe_end;
	// set timer on slotstart, interrupt must be triggered in ttpa_bitlength 
	if (ttpa_timer_set_ocr_safe(ttpa_timer_event_slotstart, ttpa_bitlength)) {
		// next slot alredy started
		bus_sendframe_end();
	}
}
	
/**
 * Guarantee that the URDE interrupt handler be called after slotstart 
 */
void bus_sendframe_end(void)
{
#endif /* TIMER_FIXEDSLOT */
	// enable UDRE interrupt
	HW_UART_STATREGB |= _BV(HW_UART_UDRIE);
	
	if(bus_hwuart_buf.timeout) {
		// set OC handler
		ttpa_sig_oc = (void (*)(void)) bus_sendframe_to;
		// set timeout to slotstart + 12 BC
		ttpa_timer_set_ocr(ttpa_timer_event_op_to);
	} else {
		ttpa_timer_oc_disable();
	}
}

#if defined(PROFILE)
ISR(BUS_SIG_IC) {
	timerval_t bus_timer_ic = TTPA_ICR;
	TTPA_TIMSK &= ~_BV(TTPA_TICIE);
	debug_profilefile.send_corr = INT16_TO_IFS(ttpa_timer_event_sendstart - bus_timer_ic);
}
#endif /*PROFILE*/

/*
 *  ISR(HW_UART_SIG_UDRE): signal handler for data register empty interrupt
 */
ISR(HW_UART_SIG_UDRE)
{ 
	// check if op. is complete
	if (bus_hwuart_buf.bytecnt == 0) { 
		bus_iobuf_t buf;

		buf.error = BUS_IO_OK;
		
		//disable timeout interrupt
		ttpa_timer_oc_disable();
		
		// disable data reg empty interrupt, enable trasmission complete interrupt
		HW_UART_STATREGB &= ~_BV(HW_UART_UDRIE);
		HW_UART_STATREGB |= _BV(HW_UART_TXCIE);
		// UART ready -> call op_complete callback
		(*bus_op_complete)(&buf);
	}
	else {
		// send next byte
		uint8_t byte = *bus_hwuart_buf.buf_ptr;
		// put data in data reg
		if (bus_hwuart_buf.par_odd) {
			if (0 == parity_even_bit(byte)) HW_UART_STATREGB |= _BV(HW_UART_TXB8);
			else HW_UART_STATREGB &= ~_BV(HW_UART_TXB8);
		}
		else {
			if (1 == parity_even_bit(byte)) HW_UART_STATREGB |= _BV(HW_UART_TXB8);
			else HW_UART_STATREGB &= ~_BV(HW_UART_TXB8);
		}
		HW_UART_DATAREG = byte;
		bus_hwuart_buf.buf_ptr++;
		bus_hwuart_buf.bytecnt--;
		// clear TXC flag
		HW_UART_STATREGA |= _BV(HW_UART_TXC);
	}
}

/*
 *  ISR(HW_UART_SIG_TXC): signal handler for transmission complete
 */
ISR(HW_UART_SIG_TXC)
{ 
	// disable TX complete interrupt
	HW_UART_STATREGB &= ~_BV(HW_UART_TXCIE);
#if defined(PROFILE)
	// disable IC send test
	TTPA_TIMSK &= ~_BV(TTPA_TICIE);
#endif /*PROFILE*/
	// set transiver direction to recive 
	bus_transcvr_recv();
}

void bus_sendframe_to(void) 
{ 
	bus_iobuf_t buf;

	DPUTC('r');
	// set transiver direction to recive 
	bus_transcvr_recv();

	buf.error = BUS_IO_TIMEOUT;
	// disable UDRE and TXC interrupts
	HW_UART_STATREGB &= ~_BV(HW_UART_TXCIE);
	HW_UART_STATREGB &= ~_BV(HW_UART_UDRIE);
	// UART ready -> call op_complete callback
	(*bus_op_complete)(&buf);
}
