/* Copyright (c) 2004, 2005, Christian Trödhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * ttpa/ttpa.c
 *      general TTP/A functions, timer handling
 *
 * $Id: ttpa.c,v 1.4 2007-01-18 19:18:47 bernd Exp $
 *
 * Revision History:
 *   2004/04/30 (0.1): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     TODO: - Jitter compensation
 *           - recv. sync. (DONE)
 *           - set timer to 0 on start of slot (DONE)
 *           - execute handling
 *           - untested: MUX, SPDUP  (DONE)
 *           - improve code size
 *   2004/05/07 (0.2): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     - added new state TTPA_STATE_PASSIVE_LOCK
 *     - changed ttpa_next_rodlentry()
 *   2004/06/09 (0.3): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     - schedule
 *   2005/08/24 (0.4): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     - added RODL entry caching
 *   2005/12/05 (0.5): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     - changed to support both, caching and non-caching version
 *   2007/01/09 (0.6): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     - added trig_init()
 *   2012/12/12 (0.7): O. Goncharov
 *     - frame oriented bus operations.  
 *     - sync on framesync (in hwuart inplementation).
 *   2012/12/14 (0.8): O. Goncharov
 *     - timer handling is moved to ttpa_timer.h
 *     - FIXEDSLOT support: set timer to zero on slot start.
 *   2012/12/25 (0.9): O. Goncharov
 *     - timer OVF detection code
 *     - don't run inittasks for any states execpt ERROR and UNSYNC.
 *     After receiving FB slave can't finish check if any inittask for current state presents on time .
 *     TODO: support for inittasks for these states.
 *     fixed rodlcache problem: update cache is not finished, when next call of next_rodlentry happens too early (DONE)
 *   2012/12/29 (0.10): O. Goncharov
 *     - fixed bug with RODL cache update priority. Now next_rodlentry is called by interrupt on mutex_release.
 *     Any user task has to wait until cache update.
 *   2013/12/29 (0.11): O. Goncharov
 *     - fixed bug with RODL chace corruption by  ttpa_emptyslot call.  
 *
 */
#include <string.h>
#include <avr/interrupt.h>
#include <util/parity.h>

#include "node_config.h"

#include "ttpa.h"
#include "ttpa_timer.h"
#include "ttpa_master.h"
#include "ttpa_slave.h"
#include "ttpa_sysfiles.h"
#include "ttpa_fb.h"
#include "bus.h"
#include "ms.h"
#include "ms_master.h"
#include "ms_slave.h"
#include "schedule.h"
#include "mutex.h"
#include "trigger.h"
#include "debug.h"

#if defined(USE_RODLCACHE)
static volatile mutex_t ttpa_re_cache_lock = { 0, 0 };

ttpa_re_cache_t ttpa_re_cache[8];

ttpa_re_cache_t ttpa_curr_frame;

static uint8_t ttpa_incsctr;

static volatile uint8_t ttpa_rodl_idx;
#else
ttpa_framestat_t ttpa_framestat;
ifs_fd_t ttpa_rodlfd;
ifs_fd_t ttpa_opfd;
#endif /* USE_RODLCACHE */

void (* volatile ttpa_sig_oc) (void);

/*
 * ISR(TTPA_SIG_OC): signal handler for output compare match interrupt
 */
ISR(TTPA_SIG_OC)
{
	uint8_t sctr = ttpa_conffile.sctr;
	// increment slot ctr on new slot
#if defined(USE_RODLCACHE)
	if(ttpa_incsctr) {
#else
	if(ttpa_framestat.incsctr) {
#endif /* USE_RODLCACHE */
		sctr++;
	}
	ttpa_conffile.sctr = sctr;
#if defined(USE_RODLCACHE)
	ttpa_incsctr = 0;
#else
	ttpa_framestat.incsctr = 0;
#endif /* USE_RODLCACHE */
	// Jitter compensation code
	// .....

	DEBUG(0, 25);
	// call handler code
	(*ttpa_sig_oc)();
}

/*
 * int ttpa_error(void): disable timer on error.
 */
int ttpa_error(void)
{
	ttpa_timer_disable();
	return 0;
}

ADD_INITTASK(task_ttpa_error, ttpa_error, 2, (1<<TTPA_STATE_ERROR));

/*
 * void ttpa_init(void): initialize TTP/A protocol: setup timers, ...
 */
int ttpa_init(void)
{
	DEBUG(2, 25);
	//DPRINT("ttpa_initi\n");
#if defined(USE_RODLCACHE)
	// initialize RODL cache
	uint8_t i;
	ifs_rodlentry_t *rptr;
	for(i = 0; i<8; i++) {
		rptr = ifs_get_re_ptr(i, 0);
		ttpa_rodl_idx = 0;
		if(rptr == NULL) {
			ttpa_re_cache[i].op = RODL_OP_INVALID;
		} else {
			switch(ttpa_rodlentry_lookup(ttpa_re_cache+i, rptr)) {
			case RE_LOOKUP_OK:
				break;
			case RE_LOOKUP_MUTEX:
			case RE_LOOKUP_PARAM:
				ttpa_setstate(TTPA_STATE_ERROR);
				break;
			case RE_LOOKUP_RODL:
				ttpa_re_cache[i].op = RODL_OP_INVALID;
			break;
			}
		}
	}
	mutex_release(&ttpa_re_cache_lock);
#endif /* USE_RODLCACHE */
	// initialize timer
	ttpa_timer_init();

	if(ttpa_conf_master()) {
		ttpa_master_init();
	} else {
		ttpa_slave_init();
	}
	trig_init();
	sei();
	return 0;
}

ADD_INITTASK(task_ttpa_init, ttpa_init, 7, (1<<TTPA_STATE_UNSYNC));

/* int ttpa_enableint(void) */
/* { */
/* 	sei(); */
/* 	return 0; */
/* } */

/* ADD_INITTASK(task_ttpa_enableint, ttpa_enableint, 8, 0xff); */

/*
 * void ttpa_recvsyncframe(void): receive-sync op. in next slot
 */
void ttpa_recvsyncframe(void)
{
	bus_iobuf_t buf;

	buf.buf = bus_io_buffer;
#if defined(USE_RODLCACHE)
	buf.spdup = ttpa_curr_frame.stat.spdup;
#else
	buf.spdup = ttpa_framestat.spdup;
#endif /* USE_RODLCACHE */
	// start with receive on BC before slotstart
	//buf.slotstart = ttpa_slotstart - ttpa_bitlength;
	// signal timeout
	buf.timeout = 1;
	// sync frame
	buf.sync = 1;
	buf.fb_byte = 0;
	DEBUG(4, 25);
	bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_recvsyncslot;
	bus_recvframe_init(&buf);
}

/*
 * void ttpa_recvsyncslot(bus_iobuf_t *busbuf): received frame from master 
 * (sync.)
 */
void ttpa_recvsyncslot(bus_iobuf_t *busbuf)
{
	// synchronize to slot start

	ttpa_recvslot(busbuf);
}

/*
 * void ttpa_recvframe(void): receive op. in next slot
 */
void ttpa_recvframe(void)
{
	bus_iobuf_t buf;

	//DPUTC('i');

	buf.buf = bus_io_buffer;
#if defined(USE_RODLCACHE)
	buf.spdup = ttpa_curr_frame.stat.spdup;
#else
	buf.spdup = ttpa_framestat.spdup;
#endif /* USE_RODLCACHE */
	// start with receive on BC before slotstart
	//buf.slotstart = ttpa_slotstart - ttpa_bitlength;
	// signal timeout
	buf.timeout = 1;
	buf.sync = 0;
	buf.fb_byte = 0;
	bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_recvslot;
	bus_recvframe_init(&buf);
}

/*
 * void ttpa_recvslot(bus_iobuf_t *busbuf): received frame
 */
void ttpa_recvslot(bus_iobuf_t *busbuf)
{
	if(busbuf->error != BUS_IO_OK) {
#if defined(USE_RODLCACHE)
		ttpa_curr_frame.stat.slotcnt = 0;
		ttpa_curr_frame.stat.framecnt = 0;
#else
		ttpa_framestat.slotcnt = 0;
		ttpa_framestat.framecnt = 0;
#endif /* USE_RODLCACHE */
		if(ttpa_conf_master()) {
			if(busbuf->error == BUS_IO_TIMEOUT && ttpa_conffile.crnd == TTPA_FB_MSD) {
				ms_msdtimeout = 1;
			}
		}
		if (busbuf->error == BUS_IO_FE) DPUTC('B');
		if (busbuf->error == BUS_IO_PE) DPUTC('P');
	} else {
#if defined(USE_RODLCACHE)
		// Add recv-buffer here
		memcpy(ttpa_curr_frame.stat.ifs_ptr, busbuf->buf, ttpa_curr_frame.stat.slotcnt);
		ttpa_curr_frame.stat.ifs_ptr += ttpa_curr_frame.stat.slotcnt;
		ttpa_curr_frame.stat.slotcnt = 0;
#else
		ifs_wr_blk(&ttpa_opfd, busbuf->buf, 0, ttpa_framestat.slotcnt);
		ttpa_opfd.buf += ttpa_framestat.slotcnt;
		ttpa_framestat.slotcnt = 0;
#endif /* USE_RODLCACHE */
		DPUTC('I');
		//DPRINT1("%x", busbuf->buf[0]);
		//DPRINT1("%x", ms_datafile.epoch);
		//DPRINT2("A%x %x", (uint16_t) &ms_datafile.epoch, (uint16_t) ttpa_curr_frame.stat.ifs_ptr);
	}
	if(ttpa_nextframe()) {
		return;
	} else {
		//TODO It seems that repeat operation with speedup does not works
		//  Repeat current operation in next time slot
		busbuf->buf = bus_io_buffer;
#if defined(USE_RODLCACHE)
		ttpa_curr_frame.stat.slotcnt = 1<<ttpa_curr_frame.stat.spdup;
		busbuf->spdup = ttpa_curr_frame.stat.spdup;
#else
		ttpa_framestat.slotcnt = 1<<ttpa_framestat.spdup;
		busbuf->spdup = ttpa_framestat.spdup;
#endif /* USE_RODLCACHE */
		//busbuf->slotstart = ttpa_slotstart - ttpa_bitlength;
		busbuf->timeout = 1;
		busbuf->sync = 0;
		busbuf->fb_byte = 0;
		bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_recvslot;
		bus_recvframe_init(busbuf);
	}
}

/*
 * void ttpa_sendframe(void): send op. in next slot
 */
void ttpa_sendframe(void)
{
	bus_iobuf_t buf;

	DPUTC('o');
#if defined(USE_RODLCACHE)
	memcpy(bus_io_buffer, ttpa_curr_frame.stat.ifs_ptr, ttpa_curr_frame.stat.slotcnt);
	ttpa_curr_frame.stat.ifs_ptr += ttpa_curr_frame.stat.slotcnt;
	ttpa_curr_frame.stat.slotcnt = 0;

	buf.buf = bus_io_buffer;
	buf.spdup = ttpa_curr_frame.stat.spdup;
#else
	ifs_rd_blk(&ttpa_opfd, bus_io_buffer, 0, ttpa_framestat.slotcnt + 1);
	ttpa_opfd.buf += ttpa_framestat.slotcnt;
	ttpa_framestat.slotcnt = 0;

	buf.buf = bus_io_buffer;
	buf.spdup = ttpa_framestat.spdup;
#endif /* USE_RODLCACHE */
	//buf.slotstart = ttpa_slotstart;
	buf.timeout = 1;
	buf.fb_byte = 0;
	bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_sendslot;
	bus_sendframe_init(&buf);
}

/*
 * void ttpa_sendslot(bus_iobuf_t *busbuf): just sent a frame
 */
void ttpa_sendslot(bus_iobuf_t *busbuf)
{
	//DPUTC('O');
	if(ttpa_nextframe()) {
		return;
	} else {
#if defined(USE_RODLCACHE)
		ttpa_curr_frame.stat.slotcnt = 1<<ttpa_curr_frame.stat.spdup;
#else
		ttpa_framestat.slotcnt = 1<<ttpa_framestat.spdup;
#endif /* USE_RODLCACHE */
		// repeat current operation in next slot
#if defined(USE_RODLCACHE)
		memcpy(bus_io_buffer, ttpa_curr_frame.stat.ifs_ptr, ttpa_curr_frame.stat.slotcnt);
		ttpa_curr_frame.stat.ifs_ptr += ttpa_curr_frame.stat.slotcnt;
		ttpa_curr_frame.stat.slotcnt = 0;

		busbuf->buf = bus_io_buffer;
		busbuf->spdup = ttpa_curr_frame.stat.spdup;
#else
		ifs_rd_blk(&ttpa_opfd, bus_io_buffer, 0, ttpa_framestat.slotcnt);
		ttpa_opfd.buf += ttpa_framestat.slotcnt;
		ttpa_framestat.slotcnt = 0;

		busbuf->buf = bus_io_buffer;
		busbuf->spdup = ttpa_framestat.spdup;
#endif /* USE_RODLCACHE */
		//busbuf->slotstart = ttpa_slotstart;
		busbuf->timeout = 1;
		busbuf->fb_byte = 0;
		bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_sendslot;
		bus_sendframe_init(busbuf);
	}
}

/*
 * void ttpa_sendpasvframe(void): listen for activity in next slot
 */
void ttpa_sendpasvframe(void)
{
	bus_iobuf_t buf;

	buf.buf = bus_io_buffer;
	buf.spdup = 0;
	// start with receive on BC before slotstart
	//buf.slotstart = ttpa_slotstart - ttpa_bitlength;
	// signal timeout
	buf.timeout = 1;
	buf.sync = 0;
	buf.fb_byte = 0;
	bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_sendpasvslot;
	bus_recvframe_init(&buf);
}


/*
 * void ttpa_sendpasvslot(bus_iobuf_t *busbuf): listen for bus activity 
 */
void ttpa_sendpasvslot(bus_iobuf_t *busbuf)
{
#if defined(USE_RODLCACHE)
	ttpa_curr_frame.stat.slotcnt = 0;
#else
	ttpa_framestat.slotcnt = 0;
#endif /* USE_RODLCACHE */
	// if no timeout has occured, an other node is sending in this slot
	if(busbuf->error != BUS_IO_TIMEOUT) {
		DPUTC('7');
		ttpa_setstate(TTPA_STATE_UNSYNC);
		return;
	} else {
		if(ttpa_nextframe()) {
			return;
		} else {
			busbuf->buf = bus_io_buffer;
			//busbuf->slotstart = ttpa_slotstart - ttpa_bitlength;
			busbuf->spdup = 0;
			busbuf->timeout = 1;
			busbuf->sync = 1;
			busbuf->fb_byte = 0;
			bus_op_complete = (void (*) (bus_iobuf_t *)) ttpa_sendpasvslot;
			bus_recvframe_init(busbuf);
		}
	}
}

/*
 * void ttpa_execframe(void): execute TTP/A task in next slot
 */
void ttpa_execframe(void)
{
	ttpa_timer_set_ocr(ttpa_timer_event_execstart);
#if defined(USE_RODLCACHE)
	ttpa_curr_frame.stat.framecnt = 0;
#else
	ttpa_framestat.framecnt = 0;
#endif /* USE_RODLCACHE */
	ttpa_sig_oc = (void (*)(void)) ttpa_execslot;
#if defined(PROFILE)
	debug_setup_tovf_check(1);
#endif /*PROFILE*/
}

/*
 * void ttpa_exectask(void): execute TTP/A task
 */
#if defined(USE_RODLCACHE)

void ttpa_execslot(void)
{
#if defined(PROFILE)
	debug_check_tovf(8);
#endif /*PROFILE*/
	ttpa_curr_frame.stat.framecnt = 0;
	// RODL cache update code has higher priority then any user task,
	// so we wait for cache update and run user task only after it
	if (mutex_set_wait(&ttpa_re_cache_lock, ttpa_exectask)) {
		//mutex capture failed, ttpa_execeortask will be called by interrupt handler
		//setup RODL cache update timeout
		ttpa_sig_oc = ttpa_rodlcache_update_to;
		ttpa_timer_set_ocr(ttpa_timer_event_op_to);
	}
}

void ttpa_exectask(void)
{
	ttpa_taskptr_t task;
	ttpa_taskparam_t param;

#if defined(PROFILE)
	debug_check_tovf(8);
#endif /*PROFILE*/
	task = ttpa_curr_frame.stat.task;
	param = ttpa_curr_frame.stat.tparam;
	//release mutex, nextframe will check it again
	mutex_release(&ttpa_re_cache_lock);
	ttpa_nextframe();
	// enable interrupts and execute user task
	sei();
	DPUTC('E');
	task(param);
	return;
}

#else /* USE_RODLCACHE */

/*
 * void ttpa_execslot(void): execute TTP/A task
 */
void ttpa_execslot(void)
{
	ttpa_taskptr_t task;
	ttpa_taskparam_t param;

#if defined(PROFILE)
	debug_check_tovf(8);
#endif /*PROFILE*/
	task = ttpa_opfd.task;
	param.rec = ttpa_opfd.addr.rec;
	param.align = ttpa_opfd.addr.align;
	ttpa_framestat.slotcnt = 0;
	ttpa_framestat.framecnt = 0;

	ttpa_nextframe();
	// enable interrupts and execute user task
	sei();
	DPUTC('E');
	task(param);
	return;
}
#endif /* USE_RODLCACHE */

/*
 * void ttpa_execeorframe(void): EOR, execute TTP/A task in IRG 
 */
void ttpa_execeorframe(void)
{
	ttpa_timer_set_ocr(ttpa_timer_event_execstart);
#if defined(USE_RODLCACHE)
	ttpa_curr_frame.stat.framecnt = 0;
#else
	ttpa_framestat.framecnt = 0;
#endif /* USE_RODLCACHE */
	ttpa_sig_oc = (void (*)(void)) ttpa_execeorslot;
#if defined(PROFILE)
	debug_setup_tovf_check(1);
#endif /*PROFILE*/
}

/*
 * void ttpa_execeorslot(void): execute TTP/A task in IRG 
 */
#if defined(USE_RODLCACHE)

void ttpa_execeorslot(void)
{
	//Cache update code has higher priority than any user task
	//we cannot run task until cache update is finished
	if (mutex_set_wait(&ttpa_re_cache_lock, ttpa_execeortask)) {
		//mutex capture failed, ttpa_execeortask will be called by interrupt handler
		//setup RODL cache update timeout
		ttpa_sig_oc = ttpa_rodlcache_update_to;
		ttpa_timer_set_ocr(ttpa_timer_event_op_to);
	} 
}

void ttpa_execeortask(void) 
{
	ttpa_taskptr_t task;
	ttpa_taskparam_t param;

#if defined(PROFILE)
	debug_check_tovf(8);
#endif /*PROFILE*/

	task = ttpa_curr_frame.stat.task;
	param = ttpa_curr_frame.stat.tparam;
	//release cache mutex, it will be tested by ttpa_nextframe again. 
	mutex_release(&ttpa_re_cache_lock);
	ttpa_irg();
	// enable interrupts and execute task
	sei();
	task(param);
	return;
}

#else

void ttpa_execeorslot(void)
{
	ttpa_taskptr_t task;
	ttpa_taskparam_t param;

#if defined(PROFILE)
	debug_check_tovf(8);
#endif /*PROFILE*/
	task = ttpa_opfd.task;
	param.rec = ttpa_opfd.addr.rec;
	param.align = ttpa_opfd.addr.align;
	ttpa_irg();
	// enable interrupts and execute task
	sei();
	task(param);
	return;
}
#endif /* USE_RODLCACHE */

/*
 * void ttpa_eor(void): end of round reached
 */
void ttpa_eor(void)
{
	DPUTC('Y');
	ttpa_timer_set_ocr(ttpa_timer_event_slotstart);
#if defined(USE_RODLCACHE)
	ttpa_curr_frame.stat.framecnt = 0;
#else
	ttpa_framestat.framecnt = 0;
#endif /* USE_RODLCACHE */
	ttpa_sig_oc = (void (*)(void)) ttpa_irg;
#if defined(PROFILE)
	debug_setup_tovf_check(1);
#endif /*PROFILE*/
}

/*
 * void ttpa_irg(void): lookup next RODL file (master) or wait for
 * next FB (slave)
 */
void ttpa_irg(void)
{
#if defined(PROFILE)
	debug_check_tovf(16);
#endif /*PROFILE*/
	if(ttpa_conf_master()) {
		ttpa_eor_master();
	} else {
		ttpa_eor_slave();
	}
	return;
}

/*
 * int ttpa_nextframe(void): returns 0 if current operation continues,
 * 1 otherwise; sets ttpa_slotstart to start of new slot
 */
#if defined(USE_RODLCACHE)
int ttpa_nextframe(void)
{
	ttpa_timer_nextslot();
	ttpa_incsctr = 1;
	if(ttpa_curr_frame.stat.framecnt > 0) {
		ttpa_curr_frame.stat.framecnt--;
		return 0;
	} else {
#if defined(PROFILE)
		debug_setup_tovf_check(0);
#endif
		//call next_rodlentry when cache update is finished
		if (mutex_set_wait(&ttpa_re_cache_lock, ttpa_next_rodlentry)) {
			//mutex capture failed, ttpa_execeortask will be called by interrupt handler
			//setup RODL cache update timeout
			//ttpa_sig_oc = ttpa_rodlcache_update_to;
			//ttpa_timer_set_ocr(ttpa_timer_event_op_to);
		}
		return 1;
	}
}
#else
int ttpa_nextframe(void)
{
	ttpa_timer_nextslot();
	ttpa_framestat.incsctr = 1;
	if(ttpa_framestat.framecnt > 0) {
		ttpa_framestat.framecnt--;
		return 0;
	} else {
#if defined(PROFILE)
		debug_setup_tovf_check(0);
#endif
		ttpa_next_rodlentry();
		return 1;
	}
}
#endif /* USE_RODLCACHE */

typedef struct {
	uint8_t mux;
#if !defined(USE_RODLCACHE)
	uint8_t mux_b2;
#endif /* USE_RODLCACHE */
	uint8_t mux_msk;
	uint8_t spdup;
	uint8_t open;
	uint8_t op;
#if !defined(USE_RODLCACHE)
	void (* func) (void);
	void (* pasv_func) (void);
#endif /* USE_RODLCACHE */
} ttpa_re_parse_t;

#if defined(USE_RODLCACHE)
const ttpa_re_parse_t __attribute__ ((progmem)) ttpa_re_parsetab[] = {
	{0x00,0x00,0,1,IFS_OP_WRITE},
	{0x00,0x00,0,1,IFS_OP_READ},
	{0x00,0x00,0,1,IFS_OP_WRITE},
	{0x00,0x00,0,1,IFS_OP_EXEC},
	{0x00,0x00,1,1,IFS_OP_WRITE},
	{0x00,0x00,1,1,IFS_OP_READ},
	{0x00,0x00,0,0,0,},
	{0x00,0x00,0,1,IFS_OP_EXEC},
	{0x00,0x03,0,1,IFS_OP_WRITE},
	{0x00,0x03,0,1,IFS_OP_READ},
	{0x00,0x07,0,1,IFS_OP_WRITE},
	{0x00,0x07,0,1,IFS_OP_READ},
	{0x00,0x03,1,1,IFS_OP_WRITE},
	{0x00,0x03,1,1,IFS_OP_READ},
	{0x04,0x07,0,1,IFS_OP_WRITE},
	{0x04,0x07,0,1,IFS_OP_READ}
};

typedef void (* ttpa_framefunc_t) (void);

const ttpa_framefunc_t PROGMEM ttpa_framefunc[] = {
	ttpa_recvframe,
	ttpa_sendframe,
	ttpa_recvsyncframe,
	ttpa_execframe,
	ttpa_recvframe,
	ttpa_sendframe,
	ttpa_eor,
	ttpa_execeorframe,
	ttpa_recvframe,
	ttpa_sendframe,
	ttpa_recvframe,
	ttpa_sendframe,
	ttpa_recvframe,
	ttpa_sendframe,
	ttpa_recvframe,
	ttpa_sendframe,
	ttpa_nextslotempty
};

const ttpa_framefunc_t PROGMEM ttpa_pasv_framefunc[] = {
	ttpa_nextslotempty,
	ttpa_sendpasvframe,
	ttpa_nextslotempty,
	ttpa_nextslotempty,
	ttpa_nextslotempty,
	ttpa_sendpasvframe,
	ttpa_eor,
	ttpa_eor,
	ttpa_nextslotempty,
	ttpa_sendpasvframe,
	ttpa_nextslotempty,
	ttpa_sendpasvframe,
	ttpa_nextslotempty,
	ttpa_sendpasvframe,
	ttpa_nextslotempty,
	ttpa_sendpasvframe,
	ttpa_nextslotempty
};
#else
const ttpa_re_parse_t __attribute__ ((progmem)) ttpa_re_parsetab[] = {
	{0,0x00,0x00,0,1,IFS_OP_WRITE,ttpa_recvframe,    ttpa_nextslotempty},
	{0,0x00,0x00,0,1,IFS_OP_READ, ttpa_sendframe,    ttpa_sendpasvframe},
	{0,0x00,0x00,0,1,IFS_OP_WRITE,ttpa_recvsyncframe,ttpa_nextslotempty},
	{0,0x00,0x00,0,1,IFS_OP_EXEC, ttpa_execframe,    ttpa_nextslotempty},
	{0,0x00,0x00,1,1,IFS_OP_WRITE,ttpa_recvframe,    ttpa_nextslotempty},
	{0,0x00,0x00,1,1,IFS_OP_READ, ttpa_sendframe,    ttpa_sendpasvframe},
	{0,0x00,0x00,0,0,0,           ttpa_eor,          ttpa_eor},
	{0,0x00,0x00,0,1,IFS_OP_EXEC, ttpa_execeorframe, ttpa_eor},
	{1,0x00,0x03,0,1,IFS_OP_WRITE,ttpa_recvframe,    ttpa_nextslotempty},
	{1,0x00,0x03,0,1,IFS_OP_READ, ttpa_sendframe,    ttpa_sendpasvframe},
	{1,0x00,0x07,0,1,IFS_OP_WRITE,ttpa_recvframe,    ttpa_nextslotempty},
	{1,0x00,0x07,0,1,IFS_OP_READ, ttpa_sendframe,    ttpa_sendpasvframe},
	{1,0x00,0x03,1,1,IFS_OP_WRITE,ttpa_recvframe,    ttpa_nextslotempty},
	{1,0x00,0x03,1,1,IFS_OP_READ, ttpa_sendframe,    ttpa_sendpasvframe},
	{1,0x04,0x07,0,1,IFS_OP_WRITE,ttpa_recvframe,    ttpa_nextslotempty},
	{1,0x04,0x07,0,1,IFS_OP_READ, ttpa_sendframe,    ttpa_sendpasvframe}
};
#endif /* USE_RODLCACHE */

#if defined(USE_RODLCACHE)
/*
 * void ttpa_next_rodlentry(void): lookup next RODL entry
 */
void ttpa_next_rodlentry(void)
{
	uint8_t update_cache = 0;
	ttpa_framefunc_t framefunc;
	ifs_rodlentry_t *rptr;
	uint8_t stat = ttpa_conffile.stat;
	uint8_t crnd = ttpa_conffile.crnd;

#if defined(PROFILE)
	debug_check_tovf(128);
#endif

	if((ttpa_conffile.sctr == 0) || (ttpa_conffile.sctr >= ttpa_curr_frame.sl_pos)) {
		// This function must be called only through mutex_set_wait() mechanism,
		// so cache mutex is raised and we can perform following operation safely 
		memcpy(&ttpa_curr_frame, ttpa_re_cache + (crnd & 0x07), sizeof(ttpa_re_cache_t));
		update_cache = 1;
		// Don't release mutex until cache is updated.
	}
	if(ttpa_curr_frame.op == RODL_OP_INVALID) {
		DPUTC('2');
		ttpa_setstate(TTPA_STATE_UNSYNC); // ev passive
	}	
	if(ttpa_conffile.sctr+1 == ttpa_curr_frame.sl_pos) {
		if(ttpa_curr_frame.mux != (ttpa_conffile.cmux & 
					   ttpa_curr_frame.mux_msk)) {
			framefunc = ttpa_nextslotempty;
			goto exec;
		}
		switch(stat) {
		case TTPA_STATE_ACTIVE:
			framefunc = (ttpa_framefunc_t) 
				pgm_read_word(ttpa_framefunc + 
					      ttpa_curr_frame.op);
			goto exec;
		case TTPA_STATE_UNBAPT:
		case TTPA_STATE_PASSIVE_LOCK:
			if((crnd == TTPA_FB_MSD) || (crnd == TTPA_FB_MSA)) {
				framefunc = (ttpa_framefunc_t) 
					pgm_read_word(ttpa_framefunc + 
						      ttpa_curr_frame.op);
				goto exec;
			} else {
				framefunc = (ttpa_framefunc_t) 
					pgm_read_word(ttpa_pasv_framefunc + 
						      ttpa_curr_frame.op);
				goto exec;
			}
		default:
			framefunc = (ttpa_framefunc_t) 
				pgm_read_word(ttpa_pasv_framefunc + 
					      ttpa_curr_frame.op);
			goto exec;
		}
	} else {
		framefunc = ttpa_nextslotempty;
		goto exec;
	}
exec:
	framefunc();
	if(update_cache) {
		sei();
		// Start cache update. Release cache mutex only when update is finished.
		// This code has a higher priority then any user task.
		if((ttpa_curr_frame.op != RODL_OP_EOR) &&
		   (ttpa_curr_frame.op != RODL_OP_EXEC_EOR)) {
			rptr = ifs_get_re_ptr((crnd&0x07), ++ttpa_rodl_idx);
		} else {
			ttpa_rodl_idx = 0;
			rptr = ifs_get_re_ptr((crnd&0x07), 0);
		}
		if(rptr == NULL) {
			ttpa_re_cache[crnd&0x07].op = RODL_OP_INVALID;
			DPUTC('3');
			ttpa_setstate(TTPA_STATE_UNSYNC); // ev passive
			return;
		}
		switch(ttpa_rodlentry_lookup(ttpa_re_cache+(crnd&0x07),rptr)) {
		case RE_LOOKUP_OK:
			break;
		case RE_LOOKUP_MUTEX: //mutex is locked
		case RE_LOOKUP_PARAM:
			ttpa_setstate(TTPA_STATE_ERROR);
			break;
		case RE_LOOKUP_RODL:
			DPUTC('4');
			ttpa_setstate(TTPA_STATE_UNSYNC); // ev passive
			break;
		}
	}
	mutex_release(&ttpa_re_cache_lock);
	return;
}

int ttpa_rodlentry_lookup(ttpa_re_cache_t *cptr, ifs_rodlentry_t *rptr)
{
	ttpa_re_parse_t parsetabentry;
	uint8_t op;
	ifs_fd_t opfd;
        ifs_addr_t addr;
	int result = RE_LOOKUP_OK;

	DEBUG(4,10);
	if((rptr == NULL) || (cptr == NULL)) {
		result = RE_LOOKUP_PARAM;
		goto cleanup;
	}
	op = rptr->op;
	memcpy_P(&parsetabentry, &ttpa_re_parsetab[op],
		 sizeof(ttpa_re_parse_t));
	cptr->op = op;
	if(parsetabentry.mux_msk) {
		cptr->mux = parsetabentry.mux | rptr->mux;
		cptr->mux_msk = parsetabentry.mux_msk;
	} else {
		cptr->mux = 0x00;
		cptr->mux_msk = 0x00;
	}
	cptr->sl_pos = rptr->sl_pos;
	addr = rptr->addr_u.addr;
	if(parsetabentry.spdup) {
		cptr->stat.spdup = addr.align+2;
		addr.align = 0;
		if(cptr->stat.spdup > MAXSPDUP) {
			cptr->op = RODL_OP_IGNORE;
			result = RE_LOOKUP_RODL;
			goto cleanup;
		}			
	} else {
		cptr->stat.spdup = 0;
	}
	cptr->stat.slotcnt = 1<<cptr->stat.spdup;
	cptr->stat.framecnt = rptr->f_len;
	if(parsetabentry.open) {
		ifs_open(&opfd, addr, IFS_REQ(IFS_ACCESS_MP, parsetabentry.op, 1));
		if(opfd.error != IFS_ERR_SUCC) {
			if(op != RODL_OP_EXEC_EOR) {
				cptr->op = RODL_OP_IGNORE;
			} else {
				cptr->op = RODL_OP_EOR;
			}
			result = RE_LOOKUP_RODL;
			goto cleanup;
		}
		if(parsetabentry.op != IFS_OP_EXEC) {
			cptr->stat.ifs_ptr = (uint8_t *) opfd.buf;
		} else {
			cptr->stat.task = opfd.task;
			cptr->stat.tparam.rec = addr.rec;
			cptr->stat.tparam.align = addr.align;
		}
	} else {
		cptr->stat.ifs_ptr = NULL;
	}
cleanup:
	//Cache update is finished, release mutex.
	mutex_release(&ttpa_re_cache_lock);
	return result;
}

/**
 * RODL cache was not updated on time
 */
void ttpa_rodlcache_update_to(void)
{
	DPUTC('U');
	// TODO: error?
	ttpa_setstate(TTPA_STATE_UNSYNC);
}

#else
/*
 * void ttpa_next_rodlentry(void): lookup next RODL entry
 */
void ttpa_next_rodlentry(void)
{
	ttpa_re_parse_t parsetabentry;
	uint8_t op;
	ifs_rodlentry_t *rodlptr;
	ifs_addr_t addr;

	rodlptr = (ifs_rodlentry_t *) ttpa_rodlfd.buf;
	if(ttpa_conffile.sctr >= rodlptr->sl_pos) {
		// test if end of RODL file is reached (error: no EOR)
		// statechange to unsync
		if(ttpa_rodlfd.len < 4) {
			DPUTC('8');
			ttpa_setstate(TTPA_STATE_UNSYNC);
			return;
		} else {
			rodlptr++;
			ttpa_rodlfd.buf = rodlptr;
			ttpa_rodlfd.len -= 4;
		}
	}
	if(ttpa_conffile.sctr+1 == rodlptr->sl_pos) {
		// parse RODL entry for next slot
		op = rodlptr->op;
		// check mux
		memcpy_P(&parsetabentry, &ttpa_re_parsetab[op],
			 sizeof(ttpa_re_parse_t));
		if(parsetabentry.mux) {
			if((uint8_t) (rodlptr->mux | parsetabentry.mux_b2) !=
			   (uint8_t) (ttpa_conffile.cmux & 
				      parsetabentry.mux_msk)) {
				ttpa_nextslotempty();
				return;
			}
		}
		addr = rodlptr->addr_u.addr;
		// check speedup
		if(parsetabentry.spdup) {
			ttpa_framestat.spdup = rodlptr->addr_u.align_spdup+2;
			addr.align = 0;
			if(ttpa_framestat.spdup > MAXSPDUP) {
				ttpa_nextslotempty();
				return;
			}			
		} else {
			ttpa_framestat.spdup = 0;
		}
		ttpa_framestat.slotcnt = 1<<ttpa_framestat.spdup;
		ttpa_framestat.framecnt = rodlptr->f_len;
		// check operation
		uint8_t stat = ttpa_conffile.stat;
		uint8_t crnd = ttpa_conffile.crnd;
		if(parsetabentry.open) {
			ifs_open(&ttpa_opfd, addr, 
				 IFS_REQ(IFS_ACCESS_MP, parsetabentry.op, 1));
			if(ttpa_opfd.error != IFS_ERR_SUCC) {
				if(op != RODL_OP_EXEC_EOR) {
					ttpa_nextslotempty();
				} else {
					ttpa_eor();
				}
				return;
			}
		}
		switch(stat) {
		case TTPA_STATE_ACTIVE:
			parsetabentry.func();
			return;
		case TTPA_STATE_UNBAPT:
		case TTPA_STATE_PASSIVE_LOCK:
			if((crnd == TTPA_FB_MSD) || (crnd == TTPA_FB_MSA)) {
				parsetabentry.func();
				return;
			} else {
				parsetabentry.pasv_func();
				return;
			}
		default:
			parsetabentry.pasv_func();
			return;
		}
	} else {
		ttpa_nextslotempty();
	}
}
#endif /* USE_RODLCACHE */

/*
 * void ttpa_nextslotempty(void): next slot is empty
 */
void ttpa_nextslotempty(void)
{
	// empty slot: set timeout
	ttpa_timer_set_ocr(ttpa_timer_event_slotstart);
	ttpa_sig_oc = (void (*)(void)) ttpa_emptyslot;
#if defined(PROFILE)
	debug_setup_tovf_check(1);
#endif /*PROFILE*/
}

/*
 * void ttpa_emptyslot(void): slot is empty, lpookup next action
 */
void ttpa_emptyslot(void)
{
	DPUTC('X');
	// next slot	
#if defined(USE_RODLCACHE)
	// we cannot call ttpa_nextframe because it decrease framecnt
	ttpa_timer_nextslot();
	ttpa_incsctr = 1;
#if defined(PROFILE)
	debug_check_tovf(32);
	debug_setup_tovf_check(0);
#endif
	//call next_rodlentry when cache update is finished
	if (mutex_set_wait(&ttpa_re_cache_lock, ttpa_next_rodlentry)) {
		//mutex capture failed, ttpa_execeortask will be called by interrupt handler
		//setup RODL cache update timeout
		//ttpa_sig_oc = ttpa_rodlcache_update_to;
		//ttpa_timer_set_ocr(ttpa_timer_event_op_to);
	}
#else
#if defined(PROFILE)
	debug_check_tovf(32);
#endif /*PROFILE*/
	ttpa_nextframe();
#endif
}

/*
 * void ttpa_setstate(uint8_t state): set protocol state and execute
 * corresponding actions
 */
#if defined(USE_SCHEDULE)
void ttpa_setstate(uint8_t state)
{
	ttpa_conffile.stat = state;
	//TODO: check if there is any inittasks for states execept UNSYNC and ERROR
	if ((state == TTPA_STATE_UNSYNC) || (state == TTPA_STATE_ERROR)) {
		schedule_inittasks();
	}
	return;
}

#else
void ttpa_setstate(uint8_t state)
{
	ttpa_conffile.stat = state;
	if(state == TTPA_STATE_UNSYNC) {
		ttpa_init();
	}
}
#endif

#if defined(USE_RODLCACHE)
/*
 * void ttpa_newround(void): FB has been received or sent, lookup RODL file
 */
void ttpa_newround(void)
{
#if defined(PROFILE)
	debug_setup_tovf_check(0);
#endif
	ttpa_rodl_idx = 0;	
	// ignore MSD round if ignore flag is set
	if(ttpa_conffile.crnd == TTPA_FB_MSD) {
                if((!ttpa_conf_master()) && (ms_datafile.msd_ignore)) {
			ttpa_re_cache[TTPA_FB_MSD&0x07].op = 
				RODL_OP_IGNORE;
		}
	}
	ttpa_conffile.sctr = 0;

	// increment epoch counter
	ttpa_conffile.ectr++;
	// if MSA round call msa_master_init or msa_slave_init
	if(ttpa_conffile.crnd == TTPA_FB_MSA) {
		//DPUTC('M');
		if(ttpa_conf_master()) {
			ms_master_init();
			if(ttpa_conffile.stat == TTPA_STATE_UNSYNC) {
				ttpa_setstate(TTPA_STATE_ACTIVE);
			}
		} else {
			ms_slave_init();
			if(ttpa_conffile.stat == TTPA_STATE_PASSIVE) {
				if(ttpa_conffile.cln == LN_NBAPT) {
					ttpa_setstate(TTPA_STATE_UNBAPT);
				} else {
					ttpa_setstate(TTPA_STATE_ACTIVE);
				}
			} else if(ttpa_conffile.stat == TTPA_STATE_UNSYNC) {
				ttpa_setstate(TTPA_STATE_PASSIVE);
			}
		}
	}
	// lookup next operation
	ttpa_curr_frame.stat.framecnt = 0;
#if defined(PROFILE)
	debug_check_tovf(128);
#endif
	ttpa_nextframe();
	return;
}
#else
/*
 * void ttpa_newround(void): FB has been received or sent, lookup RODL file
 */
void ttpa_newround(void)
{
	ifs_addr_t addr;
	// set timer to 0 on start of FB

	// lookup RODL file
	addr.filen = ttpa_conffile.crnd & 0x07;
	addr.align = 0;
	addr.rec = 1;
	ifs_close(&ttpa_rodlfd);
	ifs_open(&ttpa_rodlfd, addr, IFS_REQ(IFS_ACCESS_APPL, IFS_OP_READ, 1));
	if(ttpa_rodlfd.error != IFS_ERR_SUCC) {
		DPUTC('6');
		ttpa_setstate(TTPA_STATE_UNSYNC);
		return;
	}
	// ignore MSD round if ignore flag is set
	if(ttpa_conffile.crnd == TTPA_FB_MSD) {
		if((!ttpa_conf_master()) && (ms_datafile.msd_ignore)) {
			ttpa_rodlfd.buf += 4;
			ttpa_rodlfd.len -= 4;
		}
	}
	ttpa_conffile.sctr = 0;

	// increment epoch counter
	ttpa_conffile.ectr++;
	// if MSA round call msa_master_init or msa_slave_init
	if(ttpa_conffile.crnd == TTPA_FB_MSA) {
		if(ttpa_conf_master()) {
			ms_master_init();
			if(ttpa_conffile.stat == TTPA_STATE_UNSYNC) {
				ttpa_setstate(TTPA_STATE_ACTIVE);
			}
		} else {
			ms_slave_init();
			if(ttpa_conffile.stat == TTPA_STATE_PASSIVE) {
				if(ttpa_conffile.cln == LN_NBAPT) {
					ttpa_setstate(TTPA_STATE_UNBAPT);
				} else {
					ttpa_setstate(TTPA_STATE_ACTIVE);
				}
			} else if(ttpa_conffile.stat == TTPA_STATE_UNSYNC) {
				ttpa_setstate(TTPA_STATE_PASSIVE);
			}
		}
	}
	// lookup next operation
	ttpa_framestat.slotcnt = 0;
	ttpa_framestat.framecnt = 0;
	ttpa_nextframe();
	return;
}
#endif /* USE_RODLCACHE */
