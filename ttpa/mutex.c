/* Copyright (c) 2012, Oleg Goncharov
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * ttpa/mutex.c
 *      mutex with callback
 *
 * Revision History:
 *   2012/12/29 (0.10): O. Goncharov
 *     - first version: mutex implementation
 */

#include "mutex.h"
#include "ttpa.h"
#include "schedule.h"

volatile mutex_t * mutex_sig_ptr;
/**
 * mutex initialization
 */
int mutex_init(void) 
{
	//setup sw interupt using unconnected pin
	MUTEX_SIG_DDR |= _BV(MUTEX_SIG);
	MUTEX_SIG_PORT &= ~_BV(MUTEX_SIG);
	//setiup corresponding PCINT interrupt
	MUTEX_SIG_MSKREG |= _BV(MUTEX_SIG_MSK);
	MUTEX_SIG_IEREG &= ~_BV(MUTEX_SIG_IE);
	return 0;
}

ADD_INITTASK(task_mutex_init, mutex_init, 2, ((1<<TTPA_STATE_UNSYNC)|(1<<TTPA_STATE_ERROR)));

/**
 * interrupt handler for mutex release sw interrupt
 */
ISR(MUTEX_SIG_RELEASE)
{
	//disable mutex int
	MUTEX_SIG_IEREG &= ~_BV(MUTEX_SIG_IE);	
	//raise mutex
	mutex_sig_ptr->val = 1;
	//call callback
	(*(mutex_sig_ptr->callback))();
}
