/* Copyright (c) 2012, Oleg Goncharov
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * ttpa/bus_hwuart_slave.c
 *      HW UART functions specific for slave
 *
 * Revision History:
 *   2012/11/29 (0.1): Oleg Goncharov
 *		- Copied from bus_swuart_slave.c. FB recive based on bus_hwuart.c.
 *		- Only slotstart time is detected. ttpa_bitlength and ttpa_slotlength are predefined.
 *   2012/11/29 (0.8): Oleg Goncharov
 *      - New timer interface. Support for fixrd slot and 8-bit timer.
 */
#include <avr/interrupt.h>

#include "debug.h"
#include "node_config.h"
#include "ttpa.h"
#include "ttpa_timer.h"
#include "ttpa_fb.h"
#include "bus.h"
#include "bus_hwuart.h"
#include "bus_hwuart_slave.h"
#include "bus_transcvr.h"

void (* volatile bus_sync_op_complete) (bus_iobuf_t *param); 

/*
 * void bus_sync(void): startup synchronization for slave
 */
void bus_sync(void)
{
	ttpa_timer_set_slotlength(SLOTLENGTH_NOMINAL, BITLENGTH_NOMINAL);
	bus_set_baudrate(SLOTLENGTH_NOMINAL, BITLENGTH_NOMINAL);

	// poll until bus is high
	while(!(HW_UART_RXIN & _BV(HW_UART_RXPIN)));
	// reset timer 
#if defined(TIMER_FIXEDSLOT)
	ttpa_timer_sync(0, 0);
#endif
#if defined(TIMER_FLOATSLOT)
	ttpa_slotstart = TTPA_TCNT;
	ttpa_timer_sync(ttpa_slotstart, 0);
#endif

	// set OC handler to bus_foundirg
	ttpa_sig_oc = (void (*)(void)) bus_foundirg;
	// set OC to wait for one slot
	ttpa_timer_set_ocr(ttpa_timer_event_minslotend);
	// set IC handler to bus_sync
	bus_sig_startb = bus_sync;
	// enable startb interrupt
	BUS_SIG_RXSTART_IFREG = _BV(BUS_SIG_RXSTART_IF);
	BUS_SIG_RXSTART_ICREG |= _BV(BUS_SIG_RXSTART_IE);
}

/*
 * void bus_foundirg(void): Timeout occured, IRG has been found;
 * wait for next falling edge
 */
void bus_foundirg(void)
{
	DPUTC('G');
	// disable edge detection
	BUS_SIG_RXSTART_ICREG &= ~_BV(BUS_SIG_RXSTART_IE);
	// setup byte reception 
	bus_hwuart_buf.brreg_value = bus_hwuart_brreg_value[0];
	bus_hwuart_buf.timeout = 0; // timeout disabled, wait for falling edge
	bus_hwuart_buf.bytecnt = 1;
	bus_hwuart_buf.sync = 1;
	bus_hwuart_buf.par_odd = 1;
	bus_hwuart_buf.buf = bus_io_buffer;
	bus_hwuart_buf.buf_ptr = bus_io_buffer;
	// set startb interrupt handler
	bus_sig_startb =  bus_recvbyte_startb;
	
#if defined(PROFILE)
	// prevent false overflow detection
	debug_setup_tovf_check(0);
#endif
	
	bus_recvframe_setup();

	// save calback, setup new callback
	bus_sync_op_complete = bus_op_complete;
	bus_op_complete =  bus_syncframe;
}

/*
 * void bus_syncbyte(bus_iobuf_t *): firework byte is recived.
 */
void bus_syncframe(bus_iobuf_t * busbuf)
{
	// restore callback
	bus_op_complete = bus_sync_op_complete;

	if (busbuf->error != BUS_IO_OK) {
		bus_sync();
	}
	else {
		if (busbuf->buf[0] != TTPA_FB_MSA) {
			bus_sync();
		}
		else {
			(*bus_op_complete)(busbuf);
		}
	}
}
