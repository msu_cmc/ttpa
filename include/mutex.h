#ifndef MUTEX_H
#define MUTEX_H
/* Copyright (c) 2004, Christian Trödhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * include/mutex.h
 *      inline functions for simple mutex variables
 *
 * $Id: mutex.h,v 1.2 2007-01-16 16:52:23 bernd Exp $
 *
 * Revision History:
 *   2005/08/24 (0.4): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *     - first version
 *   2012/12/29 (0.10): O. Goncharov
 *     - release callback
 */

#include <inttypes.h>
#include <util/atomic.h>

#include "node_config.h"
#include "debug.h"

typedef struct {
	uint8_t val;
	void (* callback) (void);
} mutex_t;

// pointer for ISR routine
extern volatile mutex_t * mutex_sig_ptr;

extern int mutex_init(void);

static inline uint8_t mutex_test_set(volatile mutex_t *mutex)
{
	uint8_t ret;

	asm volatile("in __tmp_reg__,__SREG__\n\t"
			 "cli\n\t"
			 "ld %0,Z\n\t"
			 "out __SREG__,__tmp_reg__\n\t"
			 "st Z,%1\n\t"
			 : "=r" (ret) : "r" ((uint8_t) 1), "z" (&(mutex->val)) );
	return ret;
}

static inline uint8_t mutex_test(volatile mutex_t *mutex)
{
	return mutex->val;
}

static inline void mutex_release(volatile mutex_t *mutex)
{
	//TODO rewrite in assembler ?
	register uint8_t tmp = SREG;
	cli();
	mutex->val = 0;
	//setup mutex pointer for interrupt  and trigger software interrupt
	mutex_sig_ptr = mutex;
	MUTEX_SIG_PORT ^= _BV(MUTEX_SIG);
	SREG = tmp;
}

/**
 * Test mutex and setup callback on release event.
 * If mutex is set, setup callback on release interrupt and return 1. On release interrupt raise mutex and call callback.
 * If mutex is not set, raise mutex, call callback immediatly and return 0.
 * During execution of callback  interrupts are disabled and mutex is raised again.
 */
static inline uint8_t mutex_set_wait(volatile mutex_t *mutex, void (* callback)(void)) 
{
	uint8_t tmp;
	uint8_t val;

	tmp = SREG;
	cli();
	val = mutex_test(mutex);
	if (val) {
		// mutes is raised, setup interrupt
		mutex->callback = callback;
		MUTEX_SIG_IFREG = _BV(MUTEX_SIG_IE);
		MUTEX_SIG_IEREG |= _BV(MUTEX_SIG_IE);
	}
	else { 
		// mutex is released
		// set mutex
		mutex->val = 1;
		// call callback
		(*callback)();
	}
	SREG = tmp;
	return val;
}


#endif /* MUTEX_H */
