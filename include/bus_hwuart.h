#ifndef BUS_HWUART_H
#define BUS_HWUART_H
/* Copyright (c) 2012, Christian Trödhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * include/bus_hwuart.h
 *      Declarations for global bus functions and datastructures
 *
 * $Id: bus_hwuart.h,v 1.2 2012-11-26 16:55:00 bernd Exp $
 *
 * Revision History:
 *   2011/11/26 (0.1): O. Goncharov 
 *		Copied from bus_swuart.h. 
 *   2012/12/12 (0.7): O. Goncharov
 *     - frame oriented bus operations.  
 *
 */

// bus operation buffer
#ifdef OPTIMIZE_SRAM
typedef struct {
	volatile uint8_t * buf;
	volatile uint8_t * buf_ptr;
	uint8_t bytecnt;
	uint16_t brreg_value;
	unsigned par_odd : 1;
	unsigned timeout : 1;
	unsigned sync : 1;
} bus_hwuart_buf_t;
#else
typedef struct {
	volatile uint8_t * buf;
	volatile uint8_t * buf_ptr;
	uint8_t bytecnt;
	uint16_t brreg_value;
	uint8_t par_odd;
	uint8_t timeout;
	uint8_t sync;
} bus_hwuart_buf_t;
#endif /* OPTIMIZE_SRAM */

extern volatile uint16_t bus_timer_startb;

extern void (* volatile bus_sig_startb) (void);

extern uint16_t bus_hwuart_brreg_value[];

extern volatile bus_hwuart_buf_t bus_hwuart_buf;

extern void bus_set_baudrate(uint16_t slotlength, uint16_t bitlength);

extern void bus_recvframe_setup(void);

extern void bus_recvbyte_startb(void);

extern void bus_recvframe_to(void);

extern void bus_sendframe(void);

#if defined(TIMER_FIXEDSLOT)
extern void bus_sendframe_end(void);
#endif 

extern void bus_sendframe_to(void);

#endif /* BUS_HWUART_H */
