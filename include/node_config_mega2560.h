#ifndef NODE_CONFIG_MEGA2560_H
#define NODE_CONFIG_MEGA2560_H
/* Copyright (c) 2004, Christian Tr�dhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * include/node_config.h
 *      Node configuration file for atmega2560 board  
 *
 * $Id: node_config_mega2560.h 17 2012-11-29 O. Goncharov $
 *
 * Revision History:
 *   2012/11/29 (0.1): O. Goncharov
 *		- Copied from node_config_mega1280.h
 *	 2012/12/22 (0.2): O. Goncharov
 *	    - support for 8-bit timer
 *	    - max485 rs485 driver
 */

// SW configurable options
#define SWCONF_IOPORT PORTD
#define SWCONF_IODDR DDRD
#define SWCONF_SWUART PD6
#define SWCONF_AVCC PD7

#undef node_swconf_swuart
#define node_swconf_swuart() node_swconf_highen(SWCONF_SWUART) 
#undef node_swconf_hwuart
#define node_swconf_hwuart() node_swconf_lowen(SWCONF_SWUART) 
#undef node_swconf_avcc_a
#define node_swconf_avcc_a() node_swconf_lowen(SWCONF_AVCC) 
#undef node_swconf_avcc_d
#define node_swconf_avcc_d() node_swconf_highen(SWCONF_AVCC) 

#if !defined (CLOCKRATE)
//#  define CLOCKRATE 14745600
#  define CLOCKRATE 16000000
#endif /* CLOCKRATE */

#if !defined (MAXSPDUP)
#  define MAXSPDUP 3
#endif /* MAXSPDUP */

#if defined (SW_UART)
#  if !defined (SW_UART_RXPORT)
#    define SW_UART_RXIOPORT PORTL
#    define SW_UART_RXIODDR DDRL
#    define SW_UART_RXPORT PL0
#    define SW_UART_RXIOPIN PINL
#  endif /* SW_UART_RXPORT */
#  if !defined (BUS_SIG_IC)
#    define BUS_SIG_IC TIMER4_CAPT_vect 
#  endif /* BUS_SIG_IC */
#  if !defined (SW_UART_TXUSEOC)
#    define SW_UART_SENDCORR -79
#    if !defined (SW_UART_TXUSEPORT)
#      define SW_UART_TXUSEPORT
#    endif /* SW_UART_TXUSEPORT */
#  endif /* SW_UART_TXUSEOC */
#  define SW_UART_RECVCORR -77
#  if defined (SW_UART_TXUSEPORT)
#    if !defined (SW_UART_TXPORT)
#      define SW_UART_TXIOPORT PORTH
#      define SW_UART_TXIODDR DDRH
#      define SW_UART_TXPORT PH3
#    endif /* SW_UART_TXPORT */
#  endif /* SW_UART_TXUSEPORT */
#  define bus_transcvr_init() bus_transcvr_isok_init()
#  define bus_transcvr_send() bus_transcvr_isok_send()
#  define bus_transcvr_recv() bus_transcvr_isok_recv()
#endif /* UART_SW */

#if defined (HW_UART)
// RX port
#  define HW_UART_RXIOPORT PORTJ
#  define HW_UART_RXIODDR DDRJ
#  define HW_UART_RXPIN PJ0
#  define HW_UART_RXIN PINJ
// TX port
#  define HW_UART_TXPORT PORTJ
#  define HW_UART_TXDDR DDRJ
#  define HW_UART_TXPIN PJ1
// XCK
#  define HW_UART_XCKPORT PORTD
#  define HW_UART_XCKDDR DDRD
#  define HW_UART_XCKPIN PD5
// interrupts
#  if !defined (BUS_SIG_RXSTART_INT) 
#		define BUS_SIG_RXSTART_INT PCINT1_vect
//      PCINT delay + interrupt response time + delay in ISR 
#		define BUS_SIG_RXSTART_INT_CORR -51
#		define BUS_SIG_RXSTART_MSKREG PCMSK1
//TODO add mask
#		define BUS_SIG_RXSTART_MSK PCINT9
#		define BUS_SIG_RXSTART_IFREG PCIFR
#		define BUS_SIG_RXSTART_ICREG PCICR
#		define BUS_SIG_RXSTART_IE PCIE1
#		define BUS_SIG_RXSTART_IF PCIF1
#  endif /* BUS_SIG_RX_INT */
// Corrections
#  define HW_UART_RECVCORR -89
#  define UART_RECVCORR -89
#  define HW_UART_SENDCORR -157
#  define UART_SENDCORR -157
// interrupts
#  define HW_UART_SIG_RXC USART3_RX_vect
#  define HW_UART_SIG_TXC USART3_TX_vect
#  define HW_UART_SIG_UDRE USART3_UDRE_vect
// UART reisters and bits
#  define HW_UART_DATAREG UDR3
#  define HW_UART_STATREGA UCSR3A
#  define HW_UART_STATREGB UCSR3B
#  define HW_UART_STATREGC UCSR3C
#  define HW_UART_RXC RXC3
#  define HW_UART_TXC TXC3
#  define HW_UART_UDRE UDRE3
#  define HW_UART_FE FE3
#  define HW_UART_ORE DOR3 
#  define HW_UART_PE UPE3
#  define HW_UART_RXCIE RXCIE3
#  define HW_UART_TXCIE TXCIE3
#  define HW_UART_UDRIE UDRIE3
#  define HW_UART_RXEN RXEN3
#  define HW_UART_TXEN TXEN3
#  define HW_UART_RXB8 RXB83
#  define HW_UART_TXB8 TXB83
#  define HW_UART_UCSZ0 UCSZ30
#  define HW_UART_UCSZ1 UCSZ31
#  define HW_UART_UCSZ2 UCSZ32
#  define HW_UART_PM0 UPM30
#  define HW_UART_PM1 UPM31
#  define HW_UART_USBS USBS3
#  define HW_UART_U2X U2X3
#  define HW_UART_UMSEL0 UMSEL30
#  define HW_UART_UMSEL1 UMSEL31
#  define HW_UART_UCPOL UCPOL3
#  define HW_UART_BRREGL UBRR3L
#  define HW_UART_BRREGH UBRR3H
#  define HW_UART_BRREG UBRR3
#endif /* UART_HW */

#if !defined(TRANSCVR)
#  define TRANSCVR max485
#  define TRANSCVR_IOPORT PORTA
#  define TRANSCVR_IODDR DDRA
#  define TRANSCVR_PORT_TE PA0
#  define TRANSCVR_PORT_RE PA1
// bus direction control
#  define bus_transcvr_init() bus_transcvr_max485_init()
#  define bus_transcvr_send() bus_transcvr_max485_send()
#  define bus_transcvr_recv() bus_transcvr_max485_recv()
#endif /* TRANSCVR */

#if !defined(TIMER_8BIT)

#if !defined(TIMER_16BIT)
#   define TIMER_16BIT
#endif

#if !defined (TTPA_SIG_OC)
#  define TTPA_SIG_OC TIMER4_COMPB_vect
#  define TTPA_COM0 COM4B0
#  define TTPA_COM1 COM4B1
#  define TTPA_OCR OCR4B
#  define TTPA_OCIE OCIE4B
#  define TTPA_FOC FOC4B
#  if defined (SW_UART_TXUSEOC)
#    if !defined (SW_UART_TXOCPORT)
#      define SW_UART_TXOCIOPORT PORTH
#      define SW_UART_TXOCIODDR DDRH
#      define SW_UART_TXOCPORT PH4
#    endif /* SW_UART_TXOCPORT */
#  endif /* SW_UART_TXUSEOC */
#endif /* TTPA_SIG_OC */

#if !defined (TTPA_TCNT)
#  define TTPA_TCNT TCNT4
#  define TTPA_TCCRA TCCR4A
#  define TTPA_TCCRB TCCR4B
#  define TTPA_TCCRC TCCR4C
#  define TTPA_CS0 CS40
#  define TTPA_CS1 CS41
#  define TTPA_CS2 CS42
#  define TTPA_CTC WGM42
#  define TTPA_TCTCR TCCR4B
#  define TTPA_TTOP OCR4A
#  define TTPA_TIMSK TIMSK4
#  define TTPA_TIFR TIFR4
// input compare
#  define BUS_SIG_IC TIMER4_CAPT_vect 
#  define TTPA_ICR ICR4
#  define TTPA_ICES ICES4
#  define TTPA_TICIE ICIE4
// prescaler
#  define TTPA_TPCCR GTCCR
#  define TTPA_TPSR PSRSYNC
#  if defined(TIMER_FIXEDSLOT)
#    define TTPA_SIG_TOV TIMER4_COMPA_vect
#    define TTPA_TOIE OCIE4A
#  else
#    define TTPA_SIG_TOV TIMER4_OVF_vect
#    define TTPA_TOIE TOIE4
#  endif
#endif /* TTPA_TCNT */

#else /* TIMER_8BIT */

#if !defined (TTPA_SIG_OC)
#  define TTPA_SIG_OC TIMER2_COMPB_vect
#  define TTPA_OCR OCR2B
#  define TTPA_OCIE OCIE2B
#endif /* TTPA_SIG_OC */

#if !defined(TTPA_TCNT)
#  define TTPA_TCNT TCNT2
#  define TTPA_TCCRA TCCR2A
#  define TTPA_TCCRB TCCR2B
#  define TTPA_TCCRC TCCR2C
#  define TTPA_CS0 CS20
#  define TTPA_CS1 CS21
#  define TTPA_CS2 CS22
#  define TTPA_CTC WGM21
#  define TTPA_TCTCR TCCR2A
#  define TTPA_TTOP OCR2A
#  define TTPA_TIMSK TIMSK2
#  define TTPA_TIFR TIFR2
#  define TTPA_TPCCR GTCCR
#  define TTPA_TPSR PSRASY
#  if defined(TIMER_FIXEDSLOT)
#    define TTPA_SIG_TOV TIMER2_COMPA_vect
#    define TTPA_TOIE OCIE2A
#  else
#    define TTPA_SIG_TOV TIMER2_OVF_vect
#    define TTPA_TOIE TOIE2
#  endif
#endif /* TTPA_TCNT */

#endif /* TIMER_8BIT */

#define LS_SINGLE
#define LS_PORT0 PORTB
#define LS_DDR0 DDRB
#define LS_LED0 PB7

/* Mutex sw interrupt */
//use unsed pin to trigger PCINT interrupt
#if !defined (MUTEX_SIG_RELEASE) 
# 	define MUTEX_SIG_RELEASE PCINT2_vect
# 	define MUTEX_SIG_MSKREG PCMSK2
# 	define MUTEX_SIG_MSK PCINT16
# 	define MUTEX_SIG_IFREG PCIFR
# 	define MUTEX_SIG_IEREG PCICR
# 	define MUTEX_SIG_IE PCIE2
# 	define MUTEX_SIG_IF PCIF2
//  port to trigger interrupt
#   define MUTEX_SIG_PORT PORTK
#   define MUTEX_SIG_DDR DDRK
#	define MUTEX_SIG PK0
#endif /* MUTEX_SIG_RELEASE */

/* Monitoring UART configuration/ */

#define MON_UART_RX_SIG USART1_RX_vect
#define MON_UART_TX_SIG USART1_UDRE_vect
#define MON_UART_DATAREG UDR1
#define MON_UART_STATREGA UCSR1A
#define MON_UART_STATREGB UCSR1B
#define MON_UART_STATREGC UCSR1C
#define MON_UART_RXC RXC1
#define MON_UART_TXC TXC1
#define MON_UART_UDRE UDRE1
#define MON_UART_FE FE1
#define MON_UART_ORE DOR1 
#define MON_UART_PE UPE1
#define MON_UART_RXCIE RXCIE1
#define MON_UART_TXCIE TXCIE1
#define MON_UART_UDRIE UDRIE1
#define MON_UART_RXEN RXEN1
#define MON_UART_TXEN TXEN1
#define MON_UART_UCSZ0 UCSZ10
#define MON_UART_UCSZ1 UCSZ11
#define MON_UART_UCSZ2 UCSZ12
#define MON_UART_PM0 UPM10
#define MON_UART_PM1 UPM11
#define MON_UART_USBS USBS1
#define MON_UART_UBRREGL UBRR1L
#define MON_UART_UBRREGH UBRR1H

/* Monitoring timer configuration/ */
#if !defined(MON_SIG_OC)
#define MON_SIG_OC TIMER0_COMPA_vect
#define MON_SIG_TOVF TIMER0_OVF_vect
#define MON_TIFR TIFR0
#define MON_TIMSK TIMSK0
#define MON_TCNT TCNT0
#define MON_TCCR TCCR0B
#define MON_OCR OCR0A
#define MON_TOIE TOIE0
#define MON_OCIE OCIE0A
#define MON_TPRESCALE (1<<CS01)
#define PRESC_FAKT 8UL
#endif

#endif /* NODE_CONFIG_MEGA2560 */
