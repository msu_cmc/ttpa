#ifndef NODE_CONFIG_ATMEGA328P
#define NODE_CONFIG_ATMEGA328P
/* Copyright (c) 2004, 2005, Christian Tr�dhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 

/* include/node_config_mega168.h
 *      Node configuration file for board Mega168
 *
 * $Id: node_config_mega128-v2.2.h 14 2004-08-04 11:15:40Z chris $
 *
 * Revision History:
 *   2004/11/16 : S. Krywult <stefan@krywults.at>
 *     - adapted for ATmega8
 *   2005/08/24 (0.4): C. Tr�dhandl <chtr@vmars.tuwien.ac.at>
 *   2005/12/05 (0.5): C. Tr�dhandl <chtr@vmars.tuwien.ac.at>
 *     - adapted for ATmega168
 *	 2011/11/18 : O. Goncharov 
 *	   - adapted for ATmega328p
 *
 */

// SW configurable options
#define SWCONF_IOPORT PORTB
#define SWCONF_IODDR DDRB
#define SWCONF_SWUART PB2
#define SWCONF_AVCC PB3

#undef node_swconf_swuart
#define node_swconf_swuart() node_swconf_highen(SWCONF_SWUART) 
#undef node_swconf_hwuart
#define node_swconf_hwuart() node_swconf_lowen(SWCONF_SWUART) 
#undef node_swconf_avcc_a
#define node_swconf_avcc_a() node_swconf_lowen(SWCONF_AVCC) 
#undef node_swconf_avcc_d
#define node_swconf_avcc_d() node_swconf_highen(SWCONF_AVCC) 

#if !defined (CLOCKRATE)
#  define CLOCKRATE 16000000
#endif /* CLOCKRATE */

#if !defined (MAXSPDUP)
#  define MAXSPDUP 3
#endif /* MAXSPDUP */

#if defined (SW_UART)
#  if !defined (SW_UART_RXPORT)
#    define SW_UART_RXIOPORT PORTB
#    define SW_UART_RXIODDR DDRB
#    define SW_UART_RXPORT PB0
#    define SW_UART_RXIOPIN PINB
#  endif /* SW_UART_RXPORT */
#  if !defined (BUS_SIG_IC)
#    define BUS_SIG_IC TIMER1_CAPT_vect 
#  endif /* BUS_SIG_IC */
#  if !defined (SW_UART_TXUSEOC)
#    define SW_UART_SENDCORR -79
#    if !defined (SW_UART_TXUSEPORT)
#      define SW_UART_TXUSEPORT
#    endif /* SW_UART_TXUSEPORT */
#  endif /* SW_UART_TXUSEOC */
#  define SW_UART_RECVCORR -77
#  if defined (SW_UART_TXUSEPORT)
#    if !defined (SW_UART_TXPORT)
#      define SW_UART_TXIOPORT PORTB
#      define SW_UART_TXIODDR DDRB
#      define SW_UART_TXPORT PB1
#    endif /* SW_UART_TXPORT */
#  endif /* SW_UART_TXUSEPORT */
#  define bus_transcvr_init() bus_transcvr_isok_init()
#  define bus_transcvr_send() bus_transcvr_isok_send()
#  define bus_transcvr_recv() bus_transcvr_isok_recv()
#endif /* UART_SW */

#if defined (HW_UART)
// RX port
#  define HW_UART_RXIOPORT PORTD
#  define HW_UART_RXIODDR DDRD
#  define HW_UART_RXPIN PD0
#  define HW_UART_RXIN PIND
// TX port
#  define HW_UART_TXPORT PORTD
#  define HW_UART_TXDDR DDRD
#  define HW_UART_TXPIN PD1
// XCK
#  define HW_UART_XCKPORT PORTD
#  define HW_UART_XCKDDR DDRD
#  define HW_UART_XCKPIN PD4
// interrupts
#  if !defined (BUS_SIG_RXSTART_INT) 
#		define BUS_SIG_RXSTART_INT PCINT2_vect
//      PCINT delay + interrupt response time + delay in ISR 
#		define BUS_SIG_RXSTART_INT_CORR -51
#		define BUS_SIG_RXSTART_MSKREG PCMSK2
#		define BUS_SIG_RXSTART_MSK PCINT16
#		define BUS_SIG_RXSTART_IFREG PCIFR
#		define BUS_SIG_RXSTART_ICREG PCICR
#		define BUS_SIG_RXSTART_IE PCIE2
#		define BUS_SIG_RXSTART_IF PCIF2
#  endif /* BUS_SIG_RX_INT */
// Corrections
#  define HW_UART_RECVCORR -89
#  define UART_RECVCORR -89
#  define HW_UART_SENDCORR -157
#  define UART_SENDCORR -157
// interrupts
#  define HW_UART_SIG_RXC USART_RX_vect
#  define HW_UART_SIG_TXC USART_TX_vect
#  define HW_UART_SIG_UDRE USART_UDRE_vect
// UART reisters and bits
#  define HW_UART_DATAREG UDR0
#  define HW_UART_STATREGA UCSR0A
#  define HW_UART_STATREGB UCSR0B
#  define HW_UART_STATREGC UCSR0C
#  define HW_UART_RXC RXC0
#  define HW_UART_TXC TXC0
#  define HW_UART_UDRE UDRE0
#  define HW_UART_FE FE0
#  define HW_UART_ORE DOR0 
#  define HW_UART_PE UPE0
#  define HW_UART_RXCIE RXCIE0
#  define HW_UART_TXCIE TXCIE0
#  define HW_UART_UDRIE UDRIE0
#  define HW_UART_RXEN RXEN0
#  define HW_UART_TXEN TXEN0
#  define HW_UART_RXB8 RXB80
#  define HW_UART_TXB8 TXB80
#  define HW_UART_UCSZ0 UCSZ00
#  define HW_UART_UCSZ1 UCSZ01
#  define HW_UART_UCSZ2 UCSZ02
#  define HW_UART_PM0 UPM00
#  define HW_UART_PM1 UPM01
#  define HW_UART_USBS USBS0
#  define HW_UART_U2X U2X0
#  define HW_UART_UMSEL0 UMSEL00
#  define HW_UART_UMSEL1 UMSEL01
#  define HW_UART_UCPOL UCPOL0
#  define HW_UART_BRREGL UBRR0L
#  define HW_UART_BRREGH UBRR0H
#  define HW_UART_BRREG UBRR0
// bus direction control
#  if !defined(TRANSCVR)
#    define TRANSCVR max485
#    define TRANSCVR_IOPORT PORTC
#    define TRANSCVR_IODDR DDRC
#    define TRANSCVR_PORT_TE PC3
#    define TRANSCVR_PORT_RE PC2
// bus direction control
#    define bus_transcvr_init() bus_transcvr_max485_init()
#    define bus_transcvr_send() bus_transcvr_max485_send()
#    define bus_transcvr_recv() bus_transcvr_max485_recv()
#  endif /* TRANSCVR */
#endif /* UART_HW */

#if !defined(TIMER_8BIT)

#if !defined(TIMER_16BIT)
#   define TIMER_16BIT
#endif

#if !defined (TTPA_SIG_OC)
#  define TTPA_SIG_OC TIMER1_COMPB_vect 
#  define TTPA_COM0 COM1B0
#  define TTPA_COM1 COM1B1
#  define TTPA_OCR OCR1B
#  define TTPA_OCIE OCIE1B
#  define TTPA_FOC FOC1B
#  if defined (SW_UART_TXUSEOC)
#    if !defined (SW_UART_TXOCPORT)
#      define SW_UART_TXOCIOPORT PORTB
#      define SW_UART_TXOCIODDR DDRB
#      define SW_UART_TXOCPORT PB2
#    endif /* SW_UART_TXOCPORT */
#  endif /* SW_UART_TXUSEOC */
#endif /* TTPA_SIG_OC */

#if !defined (TTPA_TCNT)
#  define TTPA_TCNT TCNT1
#  define TTPA_TCCRA TCCR1A
#  define TTPA_TCCRB TCCR1B
#  define TTPA_TCCRC TCCR1C
#  define TTPA_CS0 CS10
#  define TTPA_CS1 CS11
#  define TTPA_CS2 CS12
#  define TTPA_CTC WGM12
#  define TTPA_TCTCR TCCR1B
#  define TTPA_TTOP OCR1A
#  define TTPA_TIMSK TIMSK1
#  define TTPA_TIFR TIFR1
// input compare
#  define BUS_SIG_IC TIMER1_CAPT_vect 
#  define TTPA_ICR ICR1
#  define TTPA_ICES ICES1
#  define TTPA_TICIE ICIE1
// prescaler
#  define TTPA_TPCCR GTCCR
#  define TTPA_TPSR PSRSYNC
#  if defined(TIMER_FIXEDSLOT)
#    define TTPA_SIG_TOV TIMER1_COMPA_vect
#    define TTPA_TOIE OCIE1A
#  else
#    define TTPA_SIG_TOV TIMER1_OVF_vect
#    define TTPA_TOIE TOIE1
#  endif
#endif /* TTPA_TCNT */

#else /* TIMER_8BIT */

#if !defined (TTPA_SIG_OC)
#  define TTPA_SIG_OC TIMER2_COMPB_vect
#  define TTPA_OCR OCR2B
#  define TTPA_OCIE OCIE2B
#endif /* TTPA_SIG_OC */

#if !defined(TTPA_TCNT)
#  define TTPA_TCNT TCNT2
#  define TTPA_TCCRA TCCR2A
#  define TTPA_TCCRB TCCR2B
#  define TTPA_TCCRC TCCR2C
#  define TTPA_CS0 CS20
#  define TTPA_CS1 CS21
#  define TTPA_CS2 CS22
#  define TTPA_CTC WGM21
#  define TTPA_TCTCR TCCR2A
#  define TTPA_TTOP OCR2A
#  define TTPA_TIMSK TIMSK2
#  define TTPA_TIFR TIFR2
#  define TTPA_TPCCR GTCCR
#  define TTPA_TPSR PSRASY
#  if defined(TIMER_FIXEDSLOT)
#    define TTPA_SIG_TOV TIMER2_COMPA_vect
#    define TTPA_TOIE OCIE2A
#  else
#    define TTPA_SIG_TOV TIMER2_OVF_vect
#    define TTPA_TOIE TOIE2
#  endif
#endif /* TTPA_TCNT */

#endif /* TIMER_8BIT */

#define LS_SINGLE
#define LS_PORT0 PORTB
#define LS_DDR0 DDRB
#define LS_LED0 PB5

/* Mutex sw interrupt */
//use unsed pin to trigger PCINT interrupt
//PB4(MISO) PCINT4
#if !defined (MUTEX_SIG_RELEASE) 
# 	define MUTEX_SIG_RELEASE PCINT0_vect
# 	define MUTEX_SIG_MSKREG PCMSK0
# 	define MUTEX_SIG_MSK PCINT4
# 	define MUTEX_SIG_IFREG PCIFR
# 	define MUTEX_SIG_IEREG PCICR
# 	define MUTEX_SIG_IE PCIE0
# 	define MUTEX_SIG_IF PCIF0
//  port to trigger interrupt
#   define MUTEX_SIG_PORT PORTB
#   define MUTEX_SIG_DDR DDRB
#	define MUTEX_SIG PB4
#endif /* MUTEX_SIG_RELEASE */

#if !defined(EEWE)
#define EEWE EEPE
#endif /* EEWE */

#endif /* NODE_CONFIG_ATMEGA328P */
