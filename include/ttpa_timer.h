#ifndef TTPA_TIMER_H
#define TTPA_TIMER_H
/* Copyright (c) 2012, Oleg Goncharov
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * include/ttpa_timer.h
 *      Declarations of TTP/A timer handling functions and macros 
 *
 * Revision History:
 *   2012/12/14 (0.8): O. Goncharov
 *     - Timer handling is moved from ttpa.c to this ttpa_timer.c
 *     - Added 8-bit timer support.
 *     - FIXEDSLOT mode: timer in CTC mode with TOP = slotlenght - 1, slotstart always is equal to 0.
 *   2012/12/22 (0.9): O. Goncharov
 *     - Timer overflow check in FIXEDSLOT mode throught PROFILE mechanism.
 *   2012/12/22 (0.10): O. Goncharov
 *     - Timer overflow check in FIXEDSLOT mode throught ttpa_timer_nextslot-ISR(TOVF) mechanism:
 *     If ttpa_timer_nextslot was not called one time in this current slot --- raise error. Sypport for slave sync mode.
 *     - ttpa_timer_set_ocr_safe()
 */

// global functions
#include <inttypes.h>
#include <util/delay.h>

#include "node_config.h"

#if defined(TIMER_8BIT) 
typedef uint8_t timerval_t;
#else
typedef uint16_t timerval_t;
#endif

extern volatile timerval_t ttpa_slotlength;
extern volatile timerval_t ttpa_bitlength;
extern volatile uint8_t ttpa_ovcnt;

#if defined(TIMER_FIXEDSLOT) 
// timer events
// variables contain timer value, which corresponds time, when event should happen
#define ttpa_timer_event_slotstart 0
extern timerval_t ttpa_timer_event_recvstart; // recive op. setup time
extern timerval_t ttpa_timer_event_sendstart; // send op. start time
extern timerval_t ttpa_timer_event_execstart; // exec op. start time
extern timerval_t ttpa_timer_event_startb_to; // recive op. startb timeout
extern timerval_t ttpa_timer_event_op_to; // op. in current slot timeout
extern timerval_t ttpa_timer_event_minslotend; // slotend event

// timer ovf detection
extern volatile uint8_t ttpa_timer_nextslot_cnt;

#define ttpa_timer_nextslot() do { \
	ttpa_timer_nextslot_cnt++; \
} while (0)

#else /* TIMER_FIXEDSLOT */

#if !defined(TIMER_FLOATSLOT)
#	define TIMER_FLOATSLOT
#endif

extern volatile timerval_t ttpa_slotstart;
// timer events 
#define ttpa_timer_event_slotstart ttpa_slotstart
#define ttpa_timer_event_recvstart ttpa_slotstart - ttpa_bitlength + UART_RECVCORR/TIMER_DIV - 1
#define ttpa_timer_event_sendstart ttpa_slotstart + UART_SENDCORR/TIMER_DIV - 1
#define ttpa_timer_event_execstart ttpa_slotstart
#define ttpa_timer_event_startb_to ttpa_slotstart + ttpa_bitlength
#define ttpa_timer_event_op_to ttpa_slotstart +  12.5*ttpa_bitlength
#define ttpa_timer_event_minslotend ttpa_slotstart + SLOTLENGTH_MIN/TIMER_DIV

#define ttpa_timer_nextslot() ttpa_slotstart += ttpa_slotlength

#endif /* TIMER_FIXEDSLOT */

// timer control functions

/**
 * Set OC register and enable OC interrupt.
 */
#define ttpa_timer_set_ocr(VAL) do { \
	TTPA_TIFR = _BV(TTPA_OCIE); \
	TTPA_TIMSK |= _BV(TTPA_OCIE); \
	TTPA_OCR = VAL; \
} while (0)

/**
 * Disable OC interrupt.
 */
#define ttpa_timer_oc_disable() do { \
		TTPA_TIMSK &= ~_BV(TTPA_OCIE); \
} while (0)

uint8_t ttpa_timer_set_ocr_safe(timerval_t val, timerval_t margin);

/**
 * Start timer.
 */
extern void ttpa_timer_init(void);

/**
 * Set slotlength value, recalculate timer event moments.
 */
extern void ttpa_timer_set_slotlength(uint16_t slotlength, uint16_t bitlength);

/**
 * Stop timer.
 */
extern void ttpa_timer_disable(void);

/**
 * Timer sincronization.
 */
#if (TIMER_DIV) == 1

#if defined(TIMER_FIXEDSLOT) 

#	define ttpa_timer_sync(VAL, CORR) TTPA_TCNT = VAL - CORR;

#else /* if defined(TIMER_FLOATSLOT) */

#	define ttpa_timer_sync(VAL, CORR) ttpa_slotstart = VAL + CORR;

#endif

#else
/**
 * Syncronize timer with divider. 
 * CORR is corection (minus time interval between event and start execution of macro in cycles), 
 * so CORR is always negative because it represents time delay.
 * VAL is new timer value, so after sincronization event 
 *
 * 1. Wait until for DELAY cpu cycles. DELAY = TIMER_DIV -  |CORR| % TIMER_DIV.
 * 2. Now delay between sync event and current moment is divisible by timer divider. Reset timer prescaler.
 * 3. Set timer to (|CORR| + DELAY)/TIMER_DIV. 
 */

#if defined(TIMER_FIXEDSLOT)

#define	ttpa_timer_sync(VAL, CORR) do { \
	_delay_loop_1( (TIMER_DIV - (-CORR)%(TIMER_DIV)) / 3 ); \
	TTPA_TPCCR |= _BV(TTPA_TPSR); 	  \
	TTPA_TCNT = VAL + (-CORR) / (TIMER_DIV) + 1;			  \
	ttpa_timer_8b_corr.remainer = 0; \
} while (0); 

/* 
 * Timer correction support: SLOTLENGTH is not divisible by TIMER_DIV
 */
typedef struct {
	uint8_t remainer;
	uint8_t slotshift;   /* = slotlength % TIMER_DIV */
} ttpa_timer_8b_correction_t;

extern ttpa_timer_8b_correction_t ttpa_timer_8b_corr;

#else /* if defined(TIMER_FLOATSLOT) */

#define	ttpa_timer_sync(VAL, CORR) do { \
	_delay_loop_1( (TIMER_DIV - (-CORR)%(TIMER_DIV)) / 3 ); \
	TTPA_TPCCR |= _BV(TTPA_TPSR); 	  \
	TTPA_TCNT = VAL + (-CORR) / (TIMER_DIV) + 1;			  \
	ttpa_slotstart = VAL; \
} while (0); 

#endif

#endif


#endif /* TTPA_TIMER_H */

