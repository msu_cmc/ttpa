#ifndef DEBUG_H
#define DEBUG_H
/* Copyright (c) 2004, Christian Trödhandl
   All rights reserved.
 
   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
 
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in
     the documentation and/or other materials provided with the
     distribution.
   * Neither the name of the copyright holders nor the names of
     contributors may be used to endorse or promote products derived
     from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  POSSIBILITY OF SUCH DAMAGE. */
 
/*
 * include/debug.h
 *      Debug macros
 *
 * Revision History:
 *   2004/03/30 (0.1): C. Trödhandl <chtr@vmars.tuwien.ac.at>
 *   2012/12/14 (0.7): O. Goncharov
 *     - added PUTCHAR debug interface
 *   2012/12/22 (0.9): O. Goncharov
 *     - added PROFILE: calculate corrections for send, recv and startb interrupts.
 *     - added timer OVF detection in PROFILE
 *
 */

#include "node_config.h"

extern uint8_t *debugptr;
extern volatile uint16_t debugidx;
#define DWINDOW 0x07ff

#define DEBUG_E0S(port, pin) PORT ## port &= ~(1<<P ## port ## pin)
#define DEBUG_E1S(port, pin) PORT ## port |= (1<<P ## port ## pin)
#define DEBUG_E0(port, pin) DEBUG_E0S(port, pin)
#define DEBUG_E1(port, pin) DEBUG_E1S(port, pin)
#define DEBUG_SETDDRS(port) DDR ## port = 0xff
#define DEBUG_SETDDR(port) DEBUG_SETDDRS(port)
#ifdef DEBUG_PORT
#define DEBUG_INIT() \
do { \
	DEBUG_SETDDR(DEBUG_PORT); \
} while(0)
#define DEBUG(pin, cnt) \
do { \
	uint8_t i; \
        DEBUG_E0(DEBUG_PORT, pin); \
	for(i = cnt;i;i--) { \
		asm volatile("nop" "\n\t" ::); \
	} \
	DEBUG_E1(DEBUG_PORT, pin); \
} while(0)
#define DEBUG_LOG(label,num) \
do { \
	debugptr[DWINDOW & debugidx++] = (uint8_t) (label); \
	debugptr[DWINDOW & debugidx++] = (uint8_t) (num); \
	debugptr[DWINDOW & debugidx] = (uint8_t) ('@'); \
	debugptr[DWINDOW & debugidx+1] = (uint8_t) ('@'); \
} while(0)
#else
#define DEBUG_LOG(label,num) do {} while (0)
#define DEBUG_INIT() do {} while (0)
#define DEBUG(pin, cnt) do {} while (0)
#endif /* DEBUG_PORT */

#if defined(DEBUG_PUTCHAR)
#	if !defined(DEBUG_BAUDRATE)
#		define DEBUG_BAUDRATE 57600
#	endif
#include <stdio.h>

extern void debug_init(void); 

#   define DINIT do { debug_init(); } while (0)
#   define DPUTC(c) do { putchar(c); } while (0)
#   define DPRINT(fmt) printf(fmt);
#   define DPRINT1(fmt, arg1) printf(fmt, arg1);
#   define DPRINT2(fmt, arg1, arg2) printf(fmt, arg1, arg2);
#   define DPRINT3(fmt, arg1, arg2, arg3) printf(fmt, arg1, arg2, arg3);
#else
#    define DINIT do {} while (0)
#    define DPUTC(c) do {} while (0)
#    define DPRINT(fmt) do {} while (0)
#    define DPRINT1(fmt, arg1) do {} while (0)
#    define DPRINT2(fmt, arg1, arg2) do {} while (0)
#    define DPRINT3(fmt, arg1, arg2, arg3) do {} while (0)
#endif


#if defined(PROFILE)
#if !defined(TTPA_ICR)
#    error Profiling without input capture is not possible.
#endif

#    include "ifs.h"

/* IFS structure to store profilig information */
//TODO select file numbre
//#    define DEBUG_SYSFILE_PROFILE 0x0d
#    define DEBUG_SYSFILE_PROFILE 0x10
struct debug_profilefile_struct {
	int16_t startb_corr;
	int16_t recv_corr;
	int16_t send_corr;
	int16_t recv_delay;
	uint8_t tovf_vect;
	uint8_t tovf_vect2;
	int16_t recv_end;
};
extern struct debug_profilefile_struct debug_profilefile;

/* Detection of runaway timer */
#   if defined(PROFILE_TIMER_OVF)
#       if !defined(TIMER_FIXEDSLOT)
#          error Timer overflow detection is possible only in FIXEDSLOT mode.
#       endif
extern uint8_t debug_ovcnt_expected_val;
#       define debug_setup_tovf_check(CORR) { debug_ovcnt_expected_val = ttpa_ovcnt + CORR; }
#       define debug_check_tovf(VECT) { if (ttpa_ovcnt != debug_ovcnt_expected_val) debug_profilefile.tovf_vect |= VECT; }
#else  /* DEBUG_TIMER_OVF */
#       define debug_setup_tovf_check(CORR) do {} while (0)
#       define debug_check_tovf(VECT) do {} while (0)
#    endif /* PROFILE_TIMER_OVF */
#endif /*PROFILE*/

#endif /* DEBUG_H */
